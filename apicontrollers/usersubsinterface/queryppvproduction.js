/**
 * Querying User's PPV Subscription
 */

function queryppvprod(ipadd,port,pageno,numperpage,columncode,terminalflag,isqueryvodinfo) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/queryppvprod.jsp?pageno="+pageno+"&numperpage="+numperpage+"&columncode="+columncode+"&terminalflag="+terminalflag+"&isqueryvodinfo="+isqueryvodinfo;
	var queryppvprod_request=new XMLHttpRequest();
	queryppvprod_request.open("GET", url, true);
	queryppvprod_request.send();
	queryppvprod_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			queryppvprod_request_data=JSON.parse(queryppvprod_request.responseText);
			queryppvprodCode=parseInt(queryppvprod_request_data.returncode);
			switch (queryppvprodCode) {
			case 00:
				return{
				totalcount:queryppvprod_request_data.totalcount, // Number of records 
				contenttype:queryppvprod_request_data.contenttype, // Content type 0: VOD, 1: TV, 10: Single-episode series, 14: Series head(TV series), 18: IPPV content 
				feecost:queryppvprod_request_data.feecost, // Content fee, Unit: cent 
				begintime:queryppvprod_request_data.begintime, // Subscription relationship effective time, format: YYYY.MM.DD HH:24MI:SS 
				contentname:queryppvprod_request_data.contentname, // Content name 
				endtime:queryppvprod_request_data.endtime,
				contentcode:queryppvprod_request_data.contentcode, // Content code 
				description:queryppvprod_request_data.description, // Product description 
				if_epgorder:queryppvprod_request_data.if_epgorder, // Support EPG subscription or not, 0: No, 1: Yes 
				createtime:queryppvprod_request_data.createtime, // Creation time, format: YYYY.MM.DD HH:24MI:SS 
				subusercode:queryppvprod_request_data.subusercode, // User ID of purchase account 
				productname:queryppvprod_request_data.productname, // Product name 
				columncode:queryppvprod_request_data.columncode, // Column code 
				terminalflags:queryppvprod_request_data.terminalflags, // Terminal type of product, 1: STB 2: mobile 4: PC 8: PAD support composite attribute 
				totalprice:queryppvprod_request_data.totalprice, // The total amount of consumption, unit: cent 
				definitions:queryppvprod_request_data.definitions, // Definition ID of product, 1: SD, 2: SD-H, 4: HD, support composite attribute // The following fields are information relevant to programs, that is, the fields that are returned only when isqueryvodinfo=1 
				posterfilelist:queryppvprod_request_data.posterfilelist, // Poster name list containing 12 posters separated with ";"Poster arranging order is from IPTV, MVS-Mobile, PC to MVS-Tablets, each screen with three posters including normal poster, smaller poster and big poster. 
				director:queryppvprod_request_data.director, // Director name list, separate by ‘;’ 
				actor:queryppvprod_request_data.actor, // Actor/actress name list, separate by ‘;’ 
				writer:queryppvprod_request_data.writer, // Writer list, separate by ‘;’ 
				genre:queryppvprod_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action 
				description:queryppvprod_request_data.description, // Description 
				mediaservices:queryppvprod_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
				posterpath:queryppvprod_request_data.posterpath // Relative path of program poster.
			};
				break;

			default:
				break;
			}
		}
	};
}