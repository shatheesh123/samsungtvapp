/**
 * Querying User's SVOD Subscription
 */

function querymonthprod(ipadd,port,terminalflag,pageno,numperpage,producttype) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/querymonthprod.jsp?terminalflag="+terminalflag+"&pageno="+pageno+"&numperpage="+numperpage+"&producttype="+producttype;
	var querymonthprod_request=new XMLHttpRequest();
	querymonthprod_request.open("GET", url, true);
	querymonthprod_request.send();
	querymonthprod_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			querymonthprod_request_data=JSON.parse(querymonthprod_request.responseText);
			querymonthprodCode=parseInt(querymonthprod_request_data.returncode);
			switch (querymonthprodCode) {
			case 00:
				return{
				totalcount:querymonthprod_request_data.totalcount, // Number of records 
				productcode:querymonthprod_request_data.productcode, // Product code 
				producttype:querymonthprod_request_data.producttype, // Product type, 0: Normal product, 5: Fixed-time-segment product 
				if_epgorder:querymonthprod_request_data.if_epgorder, // Support EPG subscription or not, 0: No, 1: Yes 
				autocontinue:querymonthprod_request_data.autocontinue, // Whether the subsctiption support automatic renewal, 0: No, 1: Yes 
				begintime:querymonthprod_request_data.begintime, // Subscription relationship effective time, format: YYYY.MM.DD HH:24MI:SS 
				endtime:querymonthprod_request_data.endtime, // Subscription relationship expiry time, format: YYYY.MM.DD HH:24MI:SS 
				createtime:querymonthprod_request_data.createtime, // Creation time, format: YYYY.MM.DD HH:24MI:SS 
				subusercode:querymonthprod_request_data.subusercode, // User ID of purchase account 
				productname:querymonthprod_request_data.productname, // Product name
				fee:querymonthprod_request_data.fee, // Product price, unit: cent 
				rentalterm:querymonthprod_request_data.rentalterm, // Rental term 
				terminalflags:querymonthprod_request_data.terminalflags,
			};
				break;

			default:
				break;
			}
		}
	};
	
}