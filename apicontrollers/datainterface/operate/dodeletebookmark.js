/**
 * Deleting a Bookmark
 */

function dodeletebookmark(ipadd,port,bookmarktype,contentcode,columncode) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/dodelbookmark.jsp?bookmarktype="+bookmarktype+"&contentcode="+contentcode+"&columncode="+columncode;
	var dodeletebookmark_request=new XMLHttpRequest();
	dodeletebookmark_request.open("GET", url, true);
	dodeletebookmark_request.send();
	dodeletebookmark_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			dodeletebookmark_request_data=JSON.parse(dodeletebookmark_request.responseText);
			dodeletebookmarkCode=parseInt(dodeletebookmark_request_data.returncode);
			switch (dodeletebookmarkCode) {
			case 00:
				console.log("Bookmark is deleted");
				//no responses
				break;

			default:
				return null;
				break;
			}
		}
	}
}