/**
 * Adding a Bookmark
 */

function doaddbookmark(ipadd,port,bookmarktype,contentcode,breakpoint,isshared,limitaction,terminalflag) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/doaddbookmark.jsp?bookmarktype="+bookmarktype+"&contentcode="+contentcode+"&breakpoint="+breakpoint+"&isshared="+isshared+"&limitaction="+limitaction+"&terminalflag="+terminalflag;
	var doaddbookmark_request=new XMLHttpRequest();
	doaddbookmark_request.open("GET", url, true);
	doaddbookmark_request.send();
	doaddbookmark_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			doaddbookmark_request_data=JSON.parse(doaddbookmark_request.responseText);
			doaddbookmarkCode=parseInt(doaddbookmark_request_data.returncode);
			switch (doaddbookmarkCode) {
			case 00:
				console.log("Bookmark is added");
				//no responses
				break;

			default:
				return null;
				break;
			}
		}
	};
	
}