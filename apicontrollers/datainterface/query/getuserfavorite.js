/**
 * Querying User Favorite List
 */

function getuserfavlist(ipadd,port,pageno,numperpage,favoritetype,isqueryvodinfo,unique,dirid,ordertype,sorttype,state) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getuserfavoritelist.jsp?pageno="+pageno+"&numperpage="+numperpage+"&favoritetype="+favoritetype+"&isqueryvodinfo="+isqueryvodinfo+"&unique="+unique+"&dirid="+dirid+"&ordertype="+ordertype+"&sorttype="+sorttype+"&state="+state; 
	var getuserfavlist_request=new XMLHttpRequest();
	getuserfavlist_request.open("GET", url,true);
	getuserfavlist_request.send();
	getuserfavlist_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getuserfavlist_request_data=JSON.parse(getuserfavlist_request.responseText);
			getuserfavlistCode=parseInt(getuserfavlist_request_data.returncode);
			switch (getuserfavlistCode) {
			case 00:
				return {
				totalcount:getuserfavlist_request_data.totalcount, // Number of records 
				userid:getuserfavlist_request_data.userid, // User ID (sub user ID) 
				favoritetype:getuserfavlist_request_data.favoritetype, // Favorite ype 
				contentcode:getuserfavlist_request_data.contentcode, // Content code/channel code/series head code 
				columncode:getuserfavlist_request_data.columncode, // Column code 
				isshared:getuserfavlist_request_data.isshared, // Is share，0: No, 1: Yes 
				favoritename:getuserfavlist_request_data.favoritename, // Favorite name 
				savetime:getuserfavlist_request_data.savetime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
				terminalflag:getuserfavlist_request_data.terminalflag,
				state:getuserfavlist_request_data.state, // Whether content is valid; all content is not filtered by default: 0: Invaild, such as offline or deleted. 1: Effective. 
				usermixno:getuserfavlist_request_data.usermixno, // Channel mix number for user group(displayed parameter),it’s only for channel favorite, for other favorite type, this value is -1. 
				synctimestamp:getuserfavlist_request_data.synctimestamp, 
				reserve:getuserfavlist_request_data.reserve, // Timestamp, format: YYYYMMDDHHMMSSmmm0 // reserve 
				ratingid:getuserfavlist_request_data.ratingid, //content ratingid .see General Explanatory Notes. // The following fields are information relevant to programs, that is, the fields that are returned only when isqueryvodinfo=1 
				posterfilelist:getuserfavlist_request_data.posterfilelist, // Poster name list containing 12 posters separated with ";
				director:getuserfavlist_request_data.director, // Director name list, separate by ‘;’ 
				actor:getuserfavlist_request_data.actor, // Actor/actress name list, separate by ‘;’ 
				writer:getuserfavlist_request_data.writer, // Writer list, separate by ‘;’ 
				genre:getuserfavlist_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action 
				description:getuserfavlist_request_data.description, // Description 
				mediaservices:getuserfavlist_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
				posterpath:getuserfavlist_request_data.posterpath, 
				seriestype:getuserfavlist_request_data.seriestype, // Relative path of program poster. //0:series head 1:season series head
			};
				break;

			default:
				break;
			}
		}
	};
}