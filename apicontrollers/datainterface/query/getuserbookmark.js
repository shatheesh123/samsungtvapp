/**
 * Querying a Bookmark
 */

function getuserbookmark(ipadd,port,contentcode,columncode,bookmarktype) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getuserbookmark.jsp?contentcode="+contentcode+"&columncode="+columncode+"&bookmarktype="+bookmarktype;
	var getuserbookmark_request=new XMLHttpRequest();
	getuserbookmark_request.open("GET", url, true);
	getuserbookmark_request.send();
	getuserbookmark_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getuserbookmark_request_data=JSON.parse(getuserbookmark_request.responseText);
			getuserbookmarkCode=parseInt(getuserbookmark_request_data.returncode);
			return getuserbookmark_request_data.breakpoint;
		}
		
	};
}