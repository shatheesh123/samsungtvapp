/**
 * Querying User Favorites Directory List
 */

function getuserfavoritedirlist(ipadd,port,pageno,numperpage,dirtype,unique,ordertype,sorttype) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getuserfavoritedirlist.jsp?pageno="+pageno+"&numperpage="+numperpage+"&dirtype="+dirtype+"&unique="+unique+"&ordertype="+ordertype+"&sorttype="+sorttype;
	var getuserfavoritedirlist_request=new XMLHttpRequest();
	getuserfavoritedirlist_request.open("GET", url, true);
	getuserfavoritedirlist_request.send();
	getuserfavoritedirlist_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getuserfavoritedirlist_request_data=JSON.parse(getuserfavoritedirlist_request.responseText);
			getuserfavoritedirlistCode=parseInt(getuserfavoritedirlist_request_data.returncode);
			switch (getuserfavoritedirlistCode) {
			case 00:
				return {
				totalcount:getuserfavoritedirlist_request_data.totalcount, // Number of records 
				userid:getuserfavoritedirlist_request_data.userid, // User ID (sub user ID) 
				dirtype:getuserfavoritedirlist_request_data.dirtype, // Dir type 
				dirid:getuserfavoritedirlist_request_data.dirid, // Dir ID 
				isshared:getuserfavoritedirlist_request_data.isshared, // Is share，0: No, 1: Yes 
				dirname:getuserfavoritedirlist_request_data.dirname, // Dir name 
				savetime:getuserfavoritedirlist_request_data.savetime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
				terminalflag:getuserfavoritedirlist_request_data.terminalflag, // Terminal type of creating favorite dir, 1: STB2: MOBILE4: PC8: PAD 
				synctimestamp:getuserfavoritedirlist_request_data.synctimestamp // Timestamp, format: YYYYMMDDHHMMSSmmm0
			};
				break;

			default:
				return null;
				break;
			}
		}
	};
	
}