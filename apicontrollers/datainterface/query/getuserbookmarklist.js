/**
 * Querying User Bookmark List
 */

function getuserbookmarklist(ipadd,port,pageno,numperpage,bookmarktype,isqueryvodinfo,unique,state) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getuserbookmarklist.jsp?pageno="+pageno+"&numperpage="+numperpage+"&bookmarktype="+bookmarktype+"&isqueryvodinfo="+isqueryvodinfo+"&unique="+unique+"&state="+state;
	var getuserbookmarklist_request=new XMLHttpRequest();
	getuserbookmarklist_request.open("GET", url, true);
	getuserbookmarklist_request.send();
	getuserbookmarklist_request.onreadystatechange=function(){
		getuserbookmarklist_request_data=JSON.parse(getuserbookmarklist_request.responseText);
		getuserbookmarklistCode=parseInt(getuserbookmarklist_request_data.returncode);
		switch (getuserbookmarklistCode) {
		case 00:
			return{
			totalcount:getuserbookmarklist_request_data.totalcount, // Number of records 
			userid:getuserbookmarklist_request_data.userid, // User ID (sub user ID) 
			contentcode:getuserbookmarklist_request_data.contentcode, // Content code/series head code 
			columncode:getuserbookmarklist_request_data.columncode, // Column code 
			bookmarkname:getuserbookmarklist_request_data.bookmarkname, // Bookmark name 
			isshared:getuserbookmarklist_request_data.isshared, // Is share，0: No, 1: Yes 
			bookmarktype:getuserbookmarklist_request_data.bookmarktype, // Bookmark type 
			breakpoint:getuserbookmarklist_request_data.breakpoint, // Breakpoint information, unit: second, if it is a series, the value is episode number of the series. 
			savetime:getuserbookmarklist_request_data.savetime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
			terminalflag:getuserbookmarklist_request_data.terminalflag, // Terminal type of creating bookmark, 1: STB 2: MOBILE 4: PC 8: PAD 
			state:getuserbookmarklist_request_data.state, // Whether content is valid; all content is not filtered by default: 0: Invaild, such as offline or deleted. 1: Effective. 
			synctimestamp:getuserbookmarklist_request_data.synctimestamp, //Timestamp, format: YYYYMMDDHHMMSSmmm0 // The following fields are information relevant to programs, that is, the fields that are returned only when isqueryvodinfo=1 
			posterfilelist:getuserbookmarklist_request_data.posterfilelist, // Poster name list containing 12 posters separated with ";"Poster arranging order is from IPTV, MVS-Mobile, PC to MVS-Tablets, each screen with three posters including normal poster, smaller poster and big poster. 
			director:getuserbookmarklist_request_data.director, // Director name list, separate by ‘;’ 
			actor:getuserbookmarklist_request_data.actor, // Actor/actress name list, separate by ‘;’ 
			writer:getuserbookmarklist_request_data.writer, // Writer list, separate by ‘;’ 
			genre:getuserbookmarklist_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action 
			description:getuserbookmarklist_request_data.description, // Description 
			mediaservices:getuserbookmarklist_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
			posterpath:getuserbookmarklist_request_data.posterpath, 
			seriestype:getuserbookmarklist_request_data.seriestype, // Relative path of program poster. //0:series head 1:season series head
		};			
			break;

		default:
			break;
		}
	};
}