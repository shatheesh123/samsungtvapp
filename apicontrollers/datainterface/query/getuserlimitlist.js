/**
 * Querying User Limit List
 */

function getuserlimitlist(ipadd,port,pageno,numperpage,userid,parentuserid,limittype,unique,state) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getuserlocklist.jsp?pageno="+pageno+"&numperpage="+numperpage+"&userid="+userid+"&parentuserid="+parentuserid+"&limittype="+limittype+"&unique="+unique+"&state="+state;
	var getuserlimitlist_request=new XMLHttpRequest();
	getuserlimitlist_request.open("GET", url, true);
	getuserlimitlist_request.send();
	getuserlimitlist_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getuserlimitlist_request_data=JSON.parse(getuserlimitlist_request.responseText);
			getuserlimitlistCode=parseInt(getuserlimitlist_request_data.returncode);
			switch (getuserlimitlistCode) {
			case 00:
				return {
				totalcount:getuserlimitlist_request_data.totalcount, // Number of records 
				userid:getuserlimitlist_request_data.userid, // User ID(sub user ID) 
				limitname:getuserlimitlist_request_data.limitname, // Limit name 
				contentcode:getuserlimitlist_request_data.contentcode, // Channel code 
				isshared:getuserlimitlist_request_data.isshared, // Is share，0: No, 1: Yes 
				limittype:getuserlimitlist_request_data.limittype, // Limit type 
				savetime:getuserlimitlist_request_data.savetime,
				blocktitleenable:getuserlimitlist_request_data.blocktitleenable, // Creation time, format:YYYY.MM.DD HH:24MI:SS //whether lock the title 1:yes 0: no 
				terminalflag:getuserlimitlist_request_data.terminalflag, // Terminal type of creating limit, 1: STB 2: MOBILE 4: PC 8: PAD 
				state:getuserlimitlist_request_data.state, // Whether content is valid; all content is not filtered by default: 0: Invaild, such as offline or deleted. 1: Effective. 
				synctimestamp:getuserlimitlist_request_data.synctimestamp // Timestamp, format: YYYYMMDDHHMMSSmmm0
			};
				break;

			default:
				break;
			}
		}
	};
}