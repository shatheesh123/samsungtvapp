/**
 * Query User Parent lock password List
 */

function getuserparentlocklist(ipadd,port,pageno,numperpage,userid,parentuserid,limittype,unique,state) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getuserlocklist.jsp?pageno="+pageno+"&numperpage="+numperpage+"&userid="+userid+"&parentuserid="+parentuserid+"&limittype="+limittype+"&unique="+unique+"&state="+state;
	var getuserparentlocklist_request=new XMLHttpRequest();
	getuserparentlocklist_request.open("GET", url, true);
	getuserparentlocklist_request.send();
	getuserparentlocklist_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getuserparentlocklist_request_data=JSON.parse(getuserparentlocklist_request.responseText);
			getuserparentlocklistCode=parseInt(getuserparentlocklist_request_data.returncode);
			switch (getuserparentlocklistCode) {
			case 00:
				return {
				totalcount:getuserparentlocklist_request_data.totalcount, // Number of records 
				userid:getuserparentlocklist_request_data.userid, // User ID (sub user ID) 
				limitname:getuserparentlocklist_request_data.limitname, // Limit name 
				contentcode:getuserparentlocklist_request_data.contentcode, //Content code 
				isshared:getuserparentlocklist_request_data.isshared, // Is share，0: No, 1: Yes 
				limittype:getuserparentlocklist_request_data.limittype, // Limit type 
				savetime:getuserparentlocklist_request_data.savetime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
				blocktitleenable:getuserparentlocklist_request_data.blocktitleenable, // whether lock the title 1:yes 0: no 
				terminalflag:getuserparentlocklist_request_data.terminalflag, // Terminal type of creating limit,1：STB 2：MOBILE 4：PC 8：PAD
				state:getuserparentlocklist_request_data.state, // Whether content is valid; all content is not filtered by default: 0: Invaild, such as offline or deleted. 1: Effective
				synctimestamp:getuserparentlocklist_request_data.synctimestamp,
			};
				break;

			default:
				return null;
				break;
			}
		}
	};
}