/**
 * Checking Whether Content is in Favorites
 */
function isfavorite(ipadd,port,contentcode,favoritetype,columncode) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/checkisfavorited.jsp?contentcode="+contentcode+"&favoritetype="+favoritetype+"&columncode="+columncode;
	var isfavorite_request=new XMLHttpRequest();
	isfavorite_request.open("GET", url, true);
	isfavorite_request.send();
	isfavorite_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			isfavorite_request_data=JSON.parse(isfavorite_request.responseText);
			isfavoriteCode=parseInt(isfavorite_request_data.returncode);
			switch (isfavoriteCode) {
			case 00:
				return {
				dirid:isfavorite_request_data.dirid
			};
				break;

			default:
				return null;
				break;
			}
		}
	};
}