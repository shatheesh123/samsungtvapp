/**
 * Querying User Properites
 */

function getuserproperties(ipadd,port) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getuserproperties.jsp";
	var getuserproperties_request=new XMLHttpRequest();
	getuserproperties_request.open("GET", url, true);
	getuserproperties_request.send();
	getuserproperties_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getuserproperties_request_data=JSON.parse(getuserproperties_request.responseText);
			getuserpropertiesCode=parseInt(getuserproperties_request_data.returncode);
			switch (getuserpropertiesCode) {
			case 00:
				return {
				limitpwd:getuserproperties_request_data.limitpwd, // Lock password(Limit password) 
				adultpwd:getuserproperties_request_data.adultpwd, // Adult password 
				orderpwd:getuserproperties_request_data.orderpwd, // Order password 
				limitlevel:getuserproperties_request_data.limitlevel, 
				blocktitlelevel:getuserproperties_request_data.blocktitlelevel, // User level(user rating ID), see General Explanatory Notes. // blocktitlelevel 
				switchvalue:getuserproperties_request_data.switchvalue, // Switch, each binary number represents a switch, 1: enable, 0: disable The 16th bit from the right: whether to enable the switch of the parent lock password The 17 bit from the right: whether to enable the switch of the adult password The 8 bit from the right: whether to enable the switch of the subscription password 
				userlanguage:getuserproperties_request_data.userlanguage, 
				profile:getuserproperties_request_data.profile, 
				servicecode:getuserproperties_request_data.servicecode, 
				bandwidth:getuserproperties_request_data.bandwidth, // Current user display language, the code of the Metadata display language, used to support multiple languages //user profile // servicecode // bandwidth:15000;sd:2;hd:3 "teamid":"", // Team ID
			};
				break;

			default:
				return null;
				break;
			}
		}
	};
}