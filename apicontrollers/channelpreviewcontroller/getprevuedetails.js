/**
 * Querying Information of Preview Program By Prevuecode
 */

function getprevuedetails(ipadd,port,channelcode,prevuecode) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getprevueinfo.jsp?channelcode="+channelcode+"&prevuecode="+prevuecode;
	var prevuedetails_request=new XMLHttpRequest();
	prevuedetails_request.open("GET", url, true);
	prevuedetails_request.send();
	prevuedetails_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			prevuedetails_request_data=JSON.pasre(prevuedetails_request.responseText);
			prevuedetailsCode=parseInt(prevuedetails_request_data.returncode);
			switch (prevuedetailsCode) {
			case 00:
				return{
				totalcount:prevuedetails_request_data.totalcount,// Number of records" +
				prevueid:prevuedetails_request_data.prevueid, // Preview program ID " +
				prevuecode:prevuedetails_request_data.prevuecode, // Preview program code 
				prevuename:prevuedetails_request_data.prevuename, // Preview program name 
				bocode:prevuedetails_request_data.bocode, // Service operator code 
				telecomcode:prevuedetails_request_data.telecomcode, // External code 
				mediacode:prevuedetails_request_data.mediacode, // Media provider code 
				channelcode:prevuedetails_request_data.channelcode, // Channel code 
				systemrecordenable:prevuedetails_request_data.systemrecordenable, // Support system recording or not, 0: Not supported, 1: Supported 
				privaterecordenable:prevuedetails_request_data.privaterecordenable, // Support individual recording or not, 0: Not supported, 1: Supported 
				ishot:prevuedetails_request_data.ishot, // Hot or not, 1: Yes, 0: No 
				isarchive:prevuedetails_request_data.isarchive, // Archive or not, 1: Yes, 0: No 
				hasprivaterecord:prevuedetails_request_data.hasprivaterecord, // Has NPVR record or not, 1: Yes, 0: No 
				begintime:prevuedetails_request_data.begintime, // Start time(Local time), format: YYYY.MM.DD HH:24MI:SS 
				endtime:prevuedetails_request_data.endtime, // End time(Local time), format: YYYY.MM.DD HH:24MI:SS 
				utcbegintime:prevuedetails_request_data.utcbegintime, // Start time(UTC time), format: YYYY.MM.DD HH:24MI:SS 
				utcendtime:prevuedetails_request_data.utcendtime, // End time(UTC time), format: YYYY.MM.DD HH:24MI:SS 
				savedays:prevuedetails_request_data.savedays, // Record storage time, unit: day 
				seriesheadid:prevuedetails_request_data.seriesheadid, // Series head ID, 0 by default for non-series 
				seriesname:prevuedetails_request_data.seriesname, // Series name 
				seriesvolume:prevuedetails_request_data.seriesvolume, // The total number of episodes of the series 
				seriesnum:prevuedetails_request_data.seriesnum, // Number of episodes of the series, 1 by default for non-series 
				mediaservices:prevuedetails_request_data.mediaservices, // Collection of media service type for record, these records are recorded.
				description:prevuedetails_request_data.description, // Description "createtime":[""], // Creation time, format:YYYY.MM.DD HH:24MI:SS "actor":["",""], // Actor/actress name list, separate by ‘;’ "director":["",""], // Director name list, separate by ‘;’ "releasedate":["",""], // Release date "detaildescribed":["",""], // Detail program description information(optional field) "istimeshift":["",""], // weather support to timeshift 1:yes，0:no "isarchivemode":["",""], // weather support to archive 1:yes，0:no
				isprotection:prevuedetails_request_data.isprotection, // weather support to copy protection 1:yes，0:no
				closedcaption:prevuedetails_request_data.closedcaption, // weather support to close caption 1:yes，0:no
				timeshiftmode:prevuedetails_request_data.timeshiftmode, // timeshfit mode . see General Explanatory Notes.
				archivemode:prevuedetails_request_data.archivemode, // archive mode
				copyprotection:prevuedetails_request_data.copyprotection, // copy protection
				ratingid:prevuedetails_request_data.ratingid, // content rating id .see General Explanatory Notes.
				language:prevuedetails_request_data.language, // language
				audiolangs:prevuedetails_request_data.audiolangs, // audio langs, Separate multiple by “,”
				releaseyear:prevuedetails_request_data.releaseyear, // releaseyear
				playtype:prevuedetails_request_data.playtype, // playtype 0: Premiere, 1: Replay
				programid:prevuedetails_request_data.programid, // programid
				subtitles:prevuedetails_request_data.subtitles, // subtitles , Separate multiple by “,”
				programtype:prevuedetails_request_data.programtype, // weather new program 0:yes，1:no
				programsource:prevuedetails_request_data.programsource, // program source name
				dolby:prevuedetails_request_data.dolby, // Dolby features
				tfflag:prevuedetails_request_data.tfflag, // Live recorded Flag
				premiereorfinale:prevuedetails_request_data.premiereorfinale, //
				starrating:prevuedetails_request_data.starrating, // star 0-10
				programdescription:prevuedetails_request_data.programdescription, // program description
				programlanguage:prevuedetails_request_data.programlanguage, // program language
				bitrate:prevuedetails_request_data.bitrate, // bitrate
				genre:prevuedetails_request_data.genre, // genre
				subgenre:prevuedetails_request_data.subgenre, // sub genre
				episodetitle:prevuedetails_request_data.episodetitle, // program title
				parentaladvisory:prevuedetails_request_data.parentaladvisory, // content Warning information
				tvodmode:prevuedetails_request_data.tvodmode, // catch-up TV mode . see General Explanatory Notes
			};
				break;
				
			default:
				return null;
				break;
			}
		}
	}
	
}