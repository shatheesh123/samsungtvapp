/**
 * Querying the Preview Program List of Multiple Channel
 */

function getproglistmultichannel(ipadd,port,channelcodelist,starttime,endtime,utcstarttime,utcendtime,systemrecordenable,ordertype,sorttype,isqueryrecordinfo,status,mediaservices,fields) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getmultiprevue.jsp?channelcodelist="+channelcodelist+"&starttime="+starttime+"&utcendtime="+utcendtime+"&endtime="+endtime+"&utcstarttime="+utcstarttime+"&systemrecordenable="+systemrecordenable+"&ordertype="+ordertype+"&sorttype="+sorttype+"&isqueryrecordinfo="+isqueryrecordinfo+"&status="+status+"&mediaservices="+mediaservices+"&fields="+fields;
	var proglistmultichannel_request=new XMLHttpRequest();
	proglistmultichannel_request.open("GET", url, true);
	proglistmultichannel_request.send();
	proglistmultichannel_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			proglistmultichannel_request_data=JSON.parse(proglistmultichannel_request.responseText);
			return {
				//omitted FIELDS
				status:previewproglist_request_data.status, // Recording status, 0: not recorded, 1: recorded, -1: recording failed, 998: to be recorded, 999: being recorded, -1000: no record. 
				mediaservice:previewproglist_request_datamediaservice, // Media service type of record, see General Explanatory Notes.
				recorddefinition:previewproglist_request_data
			}
		}
	};
}