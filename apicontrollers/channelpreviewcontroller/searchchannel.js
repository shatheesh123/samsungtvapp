/**
 * Searching Channel
 */

function setchannelsearch(ipadd,port,pageno,numperpage,columncode,channelname,channeltype) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/searchchannel.jsp?pageno="+pageno+"&numperpage="+numperpage+"&columncode="+columncode+"&channelname="+channelname+"&channeltype="+channelname;
	var channelsearch_request=new XMLHttpRequest();
	channelsearch_request.open("GET", url, true);
	channelsearch_request.send();
	channelsearch_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			channelsearch_request_data=JSON.parse(channelsearch_request.responseText);
			var channelsearchCode= parseInt(channelsearch_request_data.returncode);
			switch (channelsearchCode) {
			case 00:
			return {
				errormsg:channelsearch_request_data.errormsg, // Error description 
				totalcount:channelsearch_request_data.totalcount, // Number of records 
				channelcode:channelsearch_request_data.channelcode, // Channel code 
				channelname:channelsearch_request_data.channelname, // Channel name 
				mixno:channelsearch_request_data.mixno, // Global mix number(Channel number) 
				channeltype:channelsearch_request_data.channeltype, // Channel type, 0: Live, 1: NVOD channel, 3: Mosaic channel, 4: PIP channel, 5: Local Web channel, 6: Global Web channel 
				mediaservices:channelsearch_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
				ippvenable:channelsearch_request_data.ippvenable, // Support IPPV or not (ppv time segment), 1: Support, 0: Not support 
				lpvrenable:channelsearch_request_data.lpvrenable, // Support LPVR or not, 1: Support, 0: Not support 
				parentcontrolenable:channelsearch_request_data.parentcontrolenable, // Support child lock or not, 0: Support, 1: Not support 
				ratingid:channelsearch_request_data.ratingid, // Rating ID, see General Explanatory Notes. 
				sortnum:channelsearch_request_data.sortnum, // Sort number 
				bocode:channelsearch_request_data.bocode, // Service operator code 
				telecomcode:channelsearch_request_data.telecomcode, // External code 
				mediacode:channelsearch_request_data.mediacode
				};
			break;
				default:
				return null;
				break;
			}
		}
	};
	
	
}