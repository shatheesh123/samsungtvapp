/**
 * Querying Detail Information of Preview Program
 */

function getprevuedetailsinfo(ipadd,port,channelcode,prevuecode,mediaservices) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getprevuedetail.jsp?channelcode="+channelcode+"&prevuecode="+prevuecode+"&mediaservices="+mediaservices;
	var prevuedetailsinfo_request=new XMLHttpRequest();
	prevuedetailsinfo_request.open("GET", url,true);
	prevuedetailsinfo_request.send();
	prevuedetailsinfo_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			prevuedetailsinfo_request_data=JSON.parse(prevuedetailsinfo_request.responseText);
			prevuedetailsinfoCode=parseInt(prevuedetailsinfo_request_data.returncode);
			switch (prevuedetailsinfoCode) {
			case 00:
				return{
				totalcount:prevuedetailsinfo_request_data.totalcount, // Number of records 
				prevueid:prevuedetailsinfo_request_data.prevueid, // Preview program ID 
				prevuecode:prevuedetailsinfo_request_data.prevuecode,
				prevuename:prevuedetailsinfo_request_data.prevuename, // Preview program name 
				bocode:prevuedetailsinfo_request_data.bocode, // Service operator code 
				telecomcode:prevuedetailsinfo_request_data.telecomcode, // External code 
				mediacode:prevuedetailsinfo_request_data.mediacode, // Media provider code 
				channelcode:prevuedetailsinfo_request_data.channelcode, // Channel code 
				systemrecordenable:prevuedetailsinfo_request_data.systemrecordenable, // Support system recording or not, 0: Not supported, 1: Supported 
				privaterecordenable:prevuedetailsinfo_request_data.privaterecordenable, // Support individual recording or not, 0: Not supported, 1: Supported 
				ishot:prevuedetailsinfo_request_data.ishot, // Hot or not, 1: Yes, 0: No 
				isarchive:prevuedetailsinfo_request_data.isarchive, // Archive or not, 1: Yes, 0: No 
				hasprivaterecord:prevuedetailsinfo_request_data.hasprivaterecord, // Has NPVR record or not, 1: Yes, 0: No 
				begintime:prevuedetailsinfo_request_data.begintime, // Start time(Local time), format: YYYY.MM.DD HH:24MI:SS 
				endtime:prevuedetailsinfo_request_data.endtime, // End time(Local time), format: YYYY.MM.DD HH:24MI:SS 
				utcbegintime:prevuedetailsinfo_request_data.utcbegintime, // Start time(UTC time), format: YYYY.MM.DD HH:24MI:SS 
				utcendtime:prevuedetailsinfo_request_data.utcendtime, // End time(UTC time), format: YYYY.MM.DD HH:24MI:SS 
				savedays:prevuedetailsinfo_request_data.savedays, // Record storage time, unit: day 
				seriesheadid:prevuedetailsinfo_request_data.seriesheadid, // Series head ID, 0 by default for non-series 
				seriesname:prevuedetailsinfo_request_data.seriesname, // Series name 
				seriesvolume:prevuedetailsinfo_request_data.seriesvolume, // The total number of episodes of the series 
				seriesnum:prevuedetailsinfo_request_data.seriesnum, // Number of episodes of the series, 1 by default for non-series 
				mediaservices:prevuedetailsinfo_request_data.mediaservices, // Collection of media service type for record, these records are recorded. see General Explanatory Notes. 
				description:prevuedetailsinfo_request_data.description, // Description 
				createtime:prevuedetailsinfo_request_data.createtime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
				actor:prevuedetailsinfo_request_data.actor, // Actor/actress name list, separate by ‘;’ 
				director:prevuedetailsinfo_request_data.director, // Director name list, separate by ‘;’ 
				releasedate:prevuedetailsinfo_request_data.releasedate, // Release date 
				writer:prevuedetailsinfo_request_data.writer, // Writer list, separate by ‘;’ 
				detaildescribed:prevuedetailsinfo_request_data.detaildescribed, // Detail program description information(optional field) 
				istimeshift:prevuedetailsinfo_request_data.istimeshift, // weather support to timeshift 1:yes，0:no 
				isarchivemode:prevuedetailsinfo_request_data.isarchivemode, // weather support to archive 1:yes，0:no 
				isprotection:prevuedetailsinfo_request_data.isprotection, // weather support to copy protection 1:yes，0:no 
				closedcaption:prevuedetailsinfo_request_data.closedcaption, // weather support to close caption 1:yes，0:no 
				timeshiftmode:prevuedetailsinfo_request_data.timeshiftmode, // timeshfit mode . see General Explanatory Notes. 
				archivemode:prevuedetailsinfo_request_data.archivemode, // archive mode 
				copyprotection:prevuedetailsinfo_request_data.copyprotection, // copy protection 
				ratingid:prevuedetailsinfo_request_data.ratingid, // content rating id .see General Explanatory Notes. 
				language:prevuedetailsinfo_request_data.language, // language 
				audiolangs:prevuedetailsinfo_request_data.audiolangs, // audio langs, Separate multiple by “,” 
				releaseyear:prevuedetailsinfo_request_data.releaseyear, // releaseyear 
				playtype:prevuedetailsinfo_request_data.playtype, // playtype 0: Premiere, 1: Replay 
				programid:prevuedetailsinfo_request_data.programid, // programid 
				subtitles:prevuedetailsinfo_request_data.subtitles, // subtitles , Separate multiple by “,” 
				programtype:prevuedetailsinfo_request_data.programtype, // weather new program 0:yes，1:no 
				programsource:prevuedetailsinfo_request_data.programsource, // program source name
				dolby:prevuedetailsinfo_request_data.dolby, // Dolby features 
				tfflag:prevuedetailsinfo_request_data.tfflag, // Live recorded Flag 
				premiereorfinale:prevuedetailsinfo_request_data.premiereorfinale, // 
				starrating:prevuedetailsinfo_request_data.starrating, // star 0-10 
				programdescription:prevuedetailsinfo_request_data.programdescription, // program description 
				programlanguage:prevuedetailsinfo_request_data.programlanguage, // program language 
				bitrate:prevuedetailsinfo_request_data.bitrate, // bitrate 
				genre:prevuedetailsinfo_request_data.genre, // genre 
				subgenre:prevuedetailsinfo_request_data.subgenre, // sub genre 
				episodetitle:prevuedetailsinfo_request_data.episodetitle, // program title 
				parentaladvisory:prevuedetailsinfo_request_data.parentaladvisory, // content Warning information 
				tvodmode:prevuedetailsinfo_request_data.tvodmode, // catch-up TV mode . see General Explanatory Notes // The following information field for the record 
				recordcode:prevuedetailsinfo_request_data.recordcode, // Record code 
				cdnchannelcode:prevuedetailsinfo_request_data.cdnchannelcode, // Physical channel code 
				status:prevuedetailsinfo_request_data.status, // Record status, 0: Not recorded,1: Recorded, 
				mediaservice:prevuedetailsinfo_request_data.mediaservice, // Media service type of record, see General Explanatory Notes. 
				validtime:prevuedetailsinfo_request_data.validtime, // System record storage valid time (UTC time), format: YYYY.MM.DD HH:MM:SS 
				recordtelecomcode:prevuedetailsinfo_request_data.recordtelecomcode, // External code of record 
				recordmediacode:prevuedetailsinfo_request_data.recordmediacode, // Media provider code of record 
				recorddefinition:prevuedetailsinfo_request_data.recorddefinition, // Definition ID of record, 1: SD, 2: SD-H, 4: HD
			};
				break;
			default:
				return null;
				break;
			}
		}
	};
}