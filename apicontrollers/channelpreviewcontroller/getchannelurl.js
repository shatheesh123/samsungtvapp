/**
 * Querying Channel Play URL
 */

function getchannelurl(ipadd,port,channelcode,columncode,authidsession,definition,mediaservice) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getchannelurl.jsp?channelcode="+channelcode+"&columncode="+columncode+"&authidsession="+authidsession+"&definition="+definition+"&mediaservice="+mediaservice;
	var getchannel_request= new XMLHttpRequest();
	getchannel_request.open("GET", url, true);
	getchannel_request.send();
	getchannel_request.onreadystatechange=function(){
		if (getchannel_request.readyState==4 && getchannel_request.status==200) {
			getchannel_request_data=JSON.parse(getchannel_request.responseText);
			var channelCode=parseInt(getchannel_request_data.returncode);
			switch (channelCode) {
			case 00:
				return{
				errormsg : getchannel_request_data.errormsg,
				url: getchannel_request_data.url
			};
				break;

			default:
				break;
			}
		}
	};	
}