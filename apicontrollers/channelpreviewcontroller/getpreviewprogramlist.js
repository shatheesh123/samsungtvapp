/**
 * Querying the Preview Program List of a Channel
 */

function getpreviewproglist(ipadd,port,UserID,Action,TerminalFlag) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getinfobar.jsp?pageno="+pageno+"&numperpage="+numperpage+"&channelcode="+channelcode+"&starttime="+starttime+"&endtime="+endtime+"&utcstarttime="+utcstarttime+"&utcendtime="+utcendtime+"&systemrecordenable="+systemrecordenable+"&ordertype="+ordertype+"&sorttype="+sorttype+"&isqueryrecordinfo="+isqueryrecordinfo+"&status="+status+"&mediaservices="+mediaservices+"&fields="+fields;
	var previewproglist_request= new XMLHttpRequest();
	previewproglist_request.open("GET", url, true);
	previewproglist_request.send();
	previewproglist_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			previewproglist_request_data=JSON.parse(previewproglist_request.responseText);
			return {
				//omitted FIELDS
				status:previewproglist_request_data.status, // Recording status, 0: not recorded, 1: recorded, -1: recording failed, 998: to be recorded, 999: being recorded, -1000: no record. 
				mediaservice:previewproglist_request_datamediaservice, // Media service type of record, see General Explanatory Notes.
				recorddefinition:previewproglist_request_data
			};
		}
	};
}