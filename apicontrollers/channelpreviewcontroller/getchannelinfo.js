/**
 * Queryint the Channel Information
 */

function getchannelinformation(ipadd,port,channelcode,columncode,mediaservices) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getbasicchannelinfo.jsp?channelcode="+channelcode+"&columncode="+columncode+"&mediaservices="+mediaservices;
	var channelinfo_request=new XMLHttpRequest();
	channelinfo_request.open("GET", url,true);
	channelinfo_request.send();
	channelinfo_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			channelinfo_request_data=JSON.parse(channelinfo_request.responseText);
			channelinfoCode=parseInt(channelinfo_request_data.returncode);
			switch (channelinfoCode) {
			case 00:
				return{
				totalcount:channelinfo_request_data.totalcount, // Number of records " +
				channelcode:channelinfo_request_data.channelcode, // Channel code 
				channelname:channelinfo_request_data.channelname, // Channel name 
				mixno:channelinfo_request_data.mixno, // Global mix number(Channel number) 
				channeltype:channelinfo_request_data.channeltype, // Channel type, 0: Live, 1: NVOD channel, 3: Mosaic channel, 4: PIP channel, 5: Local Web channel, 6: Global Web channel 
				mediaservices:channelinfo_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
				ippvenable:channelinfo_request_data.ippvenable, // Support IPPV or not (ppv time segment), 1: Support, 0: Not support 
				lpvrenable:channelinfo_request_data.lpvrenable, // Support LPVR or not, 1: Support, 0: Not support 
				parentcontrolenable:channelinfo_request_data.parentcontrolenable, // Support child lock or not, 0: Support, 1: Not support 
				ratingid:channelinfo_request_data.ratingid, // Rating ID, see General Explanatory Notes. 
				sortnum:channelinfo_request_data.sortnum, // Sort number 
				bocode:channelinfo_request_data.bocode, // Service operator code 
				telecomcode:channelinfo_request_data.telecomcode, // External code 
				mediacode:channelinfo_request_data.mediacode, // Media provider code
				description:channelinfo_request_data.description, // Channel description 
				columncode:channelinfo_request_data.columncode, // Column code（Leaf column code） 
				columnname:channelinfo_request_data.columnname, // Column name 
				filename:channelinfo_request_data.filename, // Logo file name, relative path is:../images/markurl/ 
				audiolang:channelinfo_request_data.audiolang, // Audio language information list, separate by‘;’ 
				subtitlelang:channelinfo_request_data.subtitlelang, // Subtitle language information list, separate by‘;’ 
				usermixno:channelinfo_request_data.usermixno, // User mix number, that is display number " +
				npvravailable:channelinfo_request_data.npvravailable, // NPVR right is available or not, 0: No, 1: Yes " +
				tsavailable:channelinfo_request_data.tsavailable, // Time shift right is available or not, 0: No, >0: Yes, and the value is the time length of time shift for user team, unit: minute " +
				tvavailable:channelinfo_request_data.tvavailable,
				tvodavailable:channelinfo_request_data.tvodavailable,
				islocaltimeshift:channelinfo_request_data.islocaltimeshift,
				bitrate:channelinfo_request_data.bitrate,// Live is available or not, 0: No, 1: Yes // Catch-up right is available or not, 0: No, 1: Yes //weather support to local Timeshift 1:yes，0:no //service channel bitrate " +
				advertisecontent:channelinfo_request_data.advertisecontent
			};
				break;

			default:
				return null;
				break;
			}
		}
	};
}