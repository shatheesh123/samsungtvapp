/**
 * Querying the Channel Detail Information(By Channel Number)
 */

function getchanneldetailsnum (ipadd,port,columncode,mixno,mediaservices) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getchannelbymixno.jsp?columncode="+columncode+"&mixno="+mixno+"&mediaservices="+mediaservices;
	var channeldetailsnum_request=new XMLHttpRequest();
	channeldetailsnum_request.open("GET", url, true);
	channeldetailsnum_request.send();
	channeldetailsnum_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			channeldetailsnum_request_data=JSON.parse(channeldetailsnum_request.responseText);
			channeldetailsnumCode=parseInt(channeldetailsnum_request_data.returncode);
			switch (channeldetailsnumCode) {
			case 00:
				return{
				totalcount:channeldetails_request_data.totalcount, // Number of records 
				channelcode:channeldetails_request_data.channelcode, // Channel code 
				channelname:channeldetails_request_data.channelname, // Channel name 
				mixno:channeldetails_request_data.mixno, // Global mix number(Channel number) 
				channeltype:channeldetails_request_data.channeltype, // Channel type, 0: Live, 1: NVOD channel, 3: Mosaic channel, 4: PIP channel, 5: Local Web channel, 6: Global Web channel 
				mediaservices:channeldetails_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
				ippvenable:channeldetails_request_data.ippvenable, // Support IPPV or not (ppv time segment), 1: Support, 0: Not support 
				lpvrenable:channeldetails_request_data.lpvrenable, // Support LPVR or not, 1: Support, 0: Not support 
				parentcontrolenable:channeldetails_request_data.parentcontrolenable, // Support child lock or not, 0: Support, 1: Not support 
				ratingid:channeldetails_request_data.ratingid, // Rating ID, see General Explanatory Notes. 
				sortnum:channeldetails_request_data.sortnum, // Sort number 
				bocode:channeldetails_request_data.bocode, // Service operator code 
				telecomcode:channeldetails_request_data.telecomcode, // External code 
				mediacode:channeldetails_request_data.mediacode, // Media provider code 
				description:channeldetails_request_data.description, // Channel description 
				columncode:channeldetails_request_data.columncode, // Column code（Leaf column code）
				columnname:channeldetails_request_data.columnname, // Column name 
				filename:channeldetails_request_data.filename, // Logo file name, relative path is:../images/markurl/ 
				audiolang:channeldetails_request_data.audiolang, // Audio language information list, separate by‘;’ 
				subtitlelang:channeldetails_request_data.subtitlelang, // Subtitle language information list, separate by‘;’
				advertisecontent:channeldetails_request_data.advertisecontent, // Support ads or not, 0: No, >0: Collection of media service type. see General Explanatory Notes. // The following information field for the physical channel 
				cdnchannelcode:channeldetails_request_data.cdnchannelcode, // Physical channel code 
				mediaservice:channeldetails_request_data.mediaservice, 
				liveid:channeldetails_request_data.liveid, // Media service type of physical channel. see General Explanatory Notes. // physical channel id 
				timeshiftenable:channeldetails_request_data.timeshiftenable, // Support time shift or not, 0: Support, 1: Not support 
				tvodenable:channeldetails_request_data.tvodenable, // Support Catch-up or not, 0: Support, 1: Not support 
				shifttime:channeldetails_request_data.shifttime, // Length of time shift, unit: second 
				tvodsavetime:channeldetails_request_data.tvodsavetime, // Save time of Catch-up contents, unit: minute 
				videoratio:channeldetails_request_data.videoratio, // Video width-height ratio, such as 16:9 4:3 Undefined Widescreen 
				definition:channeldetails_request_data.definition, // Definition ID, 1: SD, 2: SD-H, 4: HD 
				cdntelecomcode:channeldetails_request_data.cdntelecomcode, // External code of physical channel 
				cdnmediacode:channeldetails_request_data.cdnmediacode, // Media provider code of physical channel 
				bitrate:channeldetails_request_data.bitrate, // Bitrate of physical channel, the default value is 2000 SD bitrate in kbps. 
				tvpauseenable:channeldetails_request_data.tvpauseenable // Support TV Pause or not, 0: Support, 1: Not support
			};
				break;

			default:
				return null;
				break;
			}
			
		}
	}
}