/**
 * Service subscription/unsubscription/subscription renewal(only for inner)
 */

function dosubscriptions(ipadd,port,purchasetype,contentcode,contenttype,productcode,columncode,isautocontinue,ordertime,definition,action,billdate) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/dosubscribe.jsp?purchasetype="+purchasetype+"&contentcode="+contentcode+"&contenttype="+contenttype+"&productcode="+productcode+"&columncode="+columncode+"&isautocontinue="+isautocontinue+"&ordertime="+ordertime+"&definition="+definition+"&action="+action+"&billdate="+billdate;
	var dosubscriptions_request=new XMLHttpRequest();
	dosubscriptions_request.open("GET", url, true);
	dosubscriptions_request.send();
	dosubscriptions_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			dosubscriptions_request_data=JSON.parse(dosubscriptions_request.responseText);
			dosubscriptionsCode=parseInt(dosubscriptions_request_data.returncode);
			switch (dosubscriptionsCode) {
			case 00:
				return {
				usertoken:dosubscriptions_request_data.usertoken, // Usertoken 
				productcode:dosubscriptions_request_data.productcode, // Product code 
				spid:dosubscriptions_request_data.spid, // SP ID 
				transactionid:dosubscriptions_request_data.transactionid, // Subscription transaction number 
				balance:dosubscriptions_request_data.balance, // Prepayment user’s balance, unit: cent 
				fee:dosubscriptions_request_data.fee,
				purchasetype:dosubscriptions_request_data.purchasetype, // Purchase type, 0: Normal fixed month product, 2: Fixed-time-segment product, 3: PPV product 
				authorizationid:dosubscriptions_request_data.authorizationid, // Authorization ID 
				expiredtime:dosubscriptions_request_data.expiredtime, // Expiration time of the subscription, format: YYYY.MM.DD HH:24MI:SS. 
				starttime:dosubscriptions_request_data.starttime, // IPPV Start time of the product time segment, format: YYYY.MM.DD HH:MM:SS 
				endtime:dosubscriptions_request_data.endtime
			};
				break;

			default:
				return null;
				break;
			}
		}
		
	};
}