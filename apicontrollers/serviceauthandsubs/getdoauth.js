/**
 * Service Authentication
 */

function doauth(ipadd,port,programcode,columncode,definition,terminalflag,contenttype,playtime,isplay) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/doauth.jsp?programcode="+programcode+"&columncode="+columncode+"&definition="+definition+"&terminalflag="+terminalflag+"&contenttype="+contenttype+"&playtime="+playtime+"&isplay="+isplay;
	var doauth_request=new XMLHttpRequest();
	doauth_request.open("GET", url, true);
	doauth_request.send();
	doauth_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			doauth_request_data=JSON.parse(doauth_request.responseText);
			doauthCode=parseInt(doauth_request_data.returncode);
			switch (doauthCode) {
			case 00:
				return{
				totalcount:doauth_request_data.totalcount, // Number of products // When authentication is successful, the subscribed product information HashMap is returned. Only one record. The following fields: 
				productcode:doauth_request_data.productcode, // Product code 
				contentcode:doauth_request_data.contentcode, // Content code 
				authorizationid:doauth_request_data.authorizationid, // Authentication ID 
				starttime:doauth_request_data.starttime, // IPPV Start time of the product time segment, format: YYYY.MM.DD HH:MM:SS 
				endtime:doauth_request_data.endtime, // IPPV End time of product time segment , format: YYYY.MM.DD HH:MM:SS
				expiredtime:doauth_request_data.expiredtime, // Subscription relationship expiry date, format:YYYY.MM.DD HH:24MI:SS 
				balance:doauth_request_data.balance, // User balance, unit: cent 
				isfree:doauth_request_data.isfree, // Free or not, 0: No, 1: Yes
				effectivedate:doauth_request_data.effectivedate, // Subscription relationship effective time, format:YYYY.MM.DD HH:24MI:SS 
				producttype:doauth_request_data.producttype, // Product type: 0: Normal product, 1: Individual space product, 2: PPV product, 3: By-time product, 4: NPVR product, 5: Fixed-time-segment product 
				prevuename:doauth_request_data.prevuename, 
				previewstarttime:doauth_request_data.previewstarttime, 
				previewendtime:doauth_request_data.previewendtime, //program name // previewstarttime ,the format is YYYY.MM.DD HH:MM:SS //previewendtime,the format is YYYY.MM.DD HH:MM:SS // When no subscription is authenticated, the available subscription product list is returned. The following fields: 
				productcode:doauth_request_data.productcode, // Product code 
				productname:doauth_request_data.productname, // Product name 
				fee:doauth_request_data.fee, // Product price, unit: cent 
				purchasetype:doauth_request_data.purchasetype, // Purchase type: 0: Normal product, 2: Fixed-time-segment product, 3: PPV product 
				productdesc:doauth_request_data.productdesc, // Product Description 
				listprice:doauth_request_data.listprice, // Nominal price, unit: cent 
				rentalterm:doauth_request_data.rentalterm, // Rental term 
				rentalunit:doauth_request_data.rentalunit, // Product rental units : 0: None, 1: Day, 2: Week 3: Month, 4: Half a year, 5: Year, 6: Hour, 7: Minute 8: Season 
				limittimes:doauth_request_data.limittimes, 
				autocontinueoption:doauth_request_data.autocontinueoption, // the number can be use // Automatic subsctiption renewal, 0: No, 1: Yes, 
				contentcode:doauth_request_data.contentcode, 
				servicecode:doauth_request_data.servicecode, // Content code // servicecode 
				starttime:doauth_request_data.starttime, // IPPV Start time of the product time segment, format: YYYY.MM.DD HH:MM:SS 
				endtime:doauth_request_data.endtime, // IPPV End time of product time segment , format: YYYY.MM.DD HH:MM:SS 
				columncode:doauth_request_data.columncode, // Column code
				terminalflags:doauth_request_data.terminalflags , // Terminal type of product, 1: STB, 2: Mobile phone, 3: PC, 4: PAD. (support the composite properties) 
				balance:doauth_request_data.balance, 
				reserve1:doauth_request_data.reserve1, 
				purchasephone:doauth_request_data.purchasephone, 
				prevuename:doauth_request_data.prevuename, 
				previewstarttime:doauth_request_data.previewstarttime, 
				previewendtime:doauth_request_data.previewendtime, 
				effecttime:doauth_request_data.effecttime, // Prepayment user’s balance, unit: cent // reserve1 //Authentication failed, return purchasephone //program name // previewstarttime ,the format is YYYY.MM.DD HH:MM:SS //previewendtime,the format is YYYY.MM.DD HH:MM:SS // The effect of time order relationship, optional field, format: YYYY.MM.DD HH:24MI:SS Note: If the value is empty, not order this product, if the value is not empty, this product has been ordered, but not effect. 
				billdate:doauth_request_data.billdate, 
				userscenetype:doauth_request_data.userscenetype, 
				chargetype:doauth_request_data.chargetype // Bill date, format: YYYY.MM.DD HH:24MI:SS. // userscenetype // chargetype 2: monthly 3: Package cycle
			};
				break;
			case 05:
				//Not subscribed, return product list.
				break;
				
			case 98:
				//Not subscribed, return product list, firstly, need user input order pin, then user can select a product to subscribe.
				break;

			default:
				break;
			}
		}
	};
}