/**
 * Adding a Favorite
 */
function addfav(ipadd,port,favoritetype,contentcode,columncode,isshared,limitaction,terminalflag,dirid) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/doaddfavorite.jsp?favoritetype="+favoritetype+"&contentcode="+contentcode+"&columncode="+columncode+"&isshared="+isshared+"&limitaction="+limitaction+"&terminalflag="+terminalflag+"&dirid="+dirid;
	var addfav_request= new XMLHttpRequest();
	addfav_request.open("GET", url,true);
	addfav_request.send();
	addfav_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			var addfav_request_data=JSON.parse(addfav_request.responseText);
			var addfavCode=parseInt(addfav_request_data.returncode);
			switch (addfavCode) {
			case 00:
				return addfav_request_data.errormsg;
				break;
			default:
				return null;
				break;
			}
		}
	};
	
}