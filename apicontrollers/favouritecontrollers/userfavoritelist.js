/**
 * Querying User Favorite List
 */

function userfavlist(ipadd,port,pageno,numperpage,favoritetype,isqueryvodinfo,unique,dirid,ordertype,sorttype,state){
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getuserfavoritelist.jsp?pageno="+pageno+"&numperpage="+numperpage+"&favoritetype="+favoritetype+"&isqueryvodinfo="+isqueryvodinfo+"&unique="+unique+"&dirid="+dirid+"&ordertype="+ordertype+"&sorttype="+sorttype+"&state="+state;
	var userfavlist_request=new XMLHttpRequest();
	userfavlist_request.open("GET", url,true);
	userfavlist_request.send();
	userfavlist_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			userfavlist_request_data=JSON.parse(userfavlist_request.responseText);
			var userfavcode=parseInt(userfavlist_request_data.returncode);
			switch (userfavcode) {
			case 00:
				return {
				errormsg:userfavlist_request_data.errormsg, // Error description 
				totalcount:userfavlist_request_data.totalcount, // Number of records 
				userid:userfavlist_request_data.userid, // User ID (sub user ID) 
				favoritetype:userfavlist_request_data.favoritetype, // Favorite type
				contentcode:userfavlist_request_data.contentcode,// Content code/channel code/series head code
				columncode:userfavlist_request_data.columncode, // Column code
				isshared:userfavlist_request_data.isshared,// Is share，0: No, 1: Yes
				favoritename:userfavlist_request_data.favoritename, // Favorite name 
				savetime:userfavlist_request_data.savetime,// Creation time, format:YYYY.MM.DD HH:24MI:SS
				terminalflag:userfavlist_request_data.terminalflag,// Terminal type of creating favorite, 1: STB 2: MOBILE
				state: userfavlist_request_data.state,// Whether content is valid; all content is not filtered by default: 0: Invaild, such as offline or deleted. 1: Effective.
				usermixno:userfavlist_request_data.usermixno, // Channel mix number for user group(displayed parameter),it’s only for channel favorite, for other favorite type, this value is -1.
				synctimestamp:userfavlist_request_data.synctimestamp ,// Timestamp, format: YYYYMMDDHHMMSSmmm0 
				reserve:userfavlist_request_data.reserve, // reserve
				ratingid:userfavlist_request_data.ratingid, //content ratingid .see General Explanatory Notes. // The following fields are information relevant to programs, that is, the fields that are returned only when isqueryvodinfo=1
				posterfilelist:userfavlist_request_data.posterfilelist, // Poster name list containing 12 posters separated with ";"Poster arranging order is from IPTV, MVS-Mobile, PC to MVS-Tablets, each screen with three posters including normal poster, smaller poster and big poster.
				director:userfavlist_request_data.director, // Director name list, separate by ‘;’ 
				actor:userfavlist_request_data.actor, // Actor/actress name list, separate by ‘;’
				writer:userfavlist_request_data.writer, // Writer list, separate by ‘;’
				genre:userfavlist_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action " +
				description:userfavlist_request_data.description, // Description 
				mediaservices:userfavlist_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes.
				posterpath:userfavlist_request_data.posterpath,
				seriestype:userfavlist_request_data.seriestype,// Relative path of program poster. //0:series head 1:season series head }
				}
				break;
			default:
				break;
			}
		}
	}
}