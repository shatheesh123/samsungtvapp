/**
 * Checking Whether Content is in Favorites
 */

function chkcontentfav(ipadd,port,contentcode,favoritetype,columncode){
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/checkisfavorited.jsp?contentcode="+contentcode+"&favoritetype="+favoritetype+"&columncode="+columncode;
	var chkfav_request=new XMLHttpRequest();
	chkfav_request.open("GET", url, true);
	chkfav_request.send();
	chkfav_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			chkfav_request_data=JSON.parse(chkfav_request.responseText);
			var chkmsgCode=parseInt(chkfav_request_data.returncode);
			switch (chkmsgCode) {
			case 00:
				return {
				errormsg:chkfav_request_data.errormsg,
				dirid:chkfav_request_data.dirid
				};s
				break;

			default:
				break;
			}
			
		}
	}
	
}