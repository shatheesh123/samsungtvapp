/**
 * Login Authentication
 */

function loginAuth(ipadd,port,UserID,UserToken,STBID,TerminalFlag,TerminalOsType){
		var url="http://"+ipadd+":"+port+"/iptvepg/datasource/mobileportalauth.jsp";
		var loginAuth_request=new XMLHttpRequest();
		var loginAuth_params={"UserID":UserID,"UserToken":UserToken,"STBID":STBID,"TerminalFlag":TerminalFlag,"TerminalOsType":TerminalOsType};
		loginAuth_request.open("POST", url,true);
		loginAuth_request.setRequestHeader("Conten-type","application/json");
		loginAuth_request.send(JSON.stringify(loginAuth_params));
		loginAuth_request.onreadystatechange=function(){
			if(this.readyState==4 && this.status==200){
				var loginAuth_request_data=JSON.parse(loginAuth_request.responseText)
				var responseCode=parseInt(loginAuth_request_data.ReturnCode);
				switch (responseCode) {
				case 00:
					return {
					ReturnCode:loginAuth_request_data.ReturnCode, // Operation result, 0: success, others: failure 
					ErrorMsg:loginAuth_request_data.ErrorMsg, // Error description 
					UserID:loginAuth_request_data.UserID, // Sub-account ID 
					FatherUserID:loginAuth_request_data.FatherUserID, // Parent account ID 
					UserIP:loginAuth_request_data.UserIP, // Client IP address 
					STBID:loginAuth_request_data.STBID, // Device ID 
					VendorID:loginAuth_request_data.VendorID, // Vendor ID 
					UserToken:loginAuth_request_data.UserToken, // Usertoken 
					EpgServerIP:loginAuth_request_data.EpgServerIP, // Service EPG IP address 
					IsSupportLock:loginAuth_request_data.IsSupportLock, // Whether the EPG supports the parent lock switch, 1: yes; 0: no 
					PreviewMsecond:loginAuth_request_data.PreviewMsecond, // Channel preview time, unit: ms 
					ChannelSwitchmode:loginAuth_request_data.ChannelSwitchmode, // Channel switch mode (system configuration), 1: Full channel switch, 0: Switch between subscribed channels 
					LoginDate:loginAuth_request_data.LoginDate, // User login time, format: yyyy.MM.dd HH:mm:ss 
					SkinType:loginAuth_request_data.SkinType, // User template skin information //// vcdn ID 
					AreaNO:loginAuth_request_data.AreaNO, // User area code 
					TradeID:loginAuth_request_data.TradeID, // User industry information 
					UserStatus:loginAuth_request_data.UserStatus, // User status, 0: normal, 1: to be activated, 2: owing the fees, 3: suspension, 4: shutdown
					StypeUrl:loginAuth_request_data.StypeUrl, // Template code, such as framexxx 
					BoCode:loginAuth_request_data.BoCode, // Service operation code 
					TeamID:loginAuth_request_data.TeamID, // Team ID 
					LimitLevel:loginAuth_request_data.LimitLevel, // User limit level 
					LimitPwd:loginAuth_request_data.LimitPwd, // User lock password 
					CurChannel:loginAuth_request_data.CurChannel, // Channel code accessed by the user recently or start channel. 
					CityCode:loginAuth_request_data.CityCode, //City code 
					TokenExpiredTime:loginAuth_request_data.TokenExpiredTime, // Invalid time of the UserToken, format: yyyy.MM.dd HH:mm:ss 
					ServNodeID:loginAuth_request_data.ServNodeID, // Service node ID 
					ServNodeIP:loginAuth_request_data.ServNodeIP, // Service node IP address 
					ServNodePort:loginAuth_request_data.ServNodePort, // Service node port 
					ServNodeType:loginAuth_request_data.ServNodeType, 
					UserLiveType:loginAuth_request_data.UserLiveType, // Service node type, 0:POP node, 1:USS node //User Live Type 0:only support unicast 1:support unicast and multicast 
					StbMac:loginAuth_request_data.StbMac, // MAC address 
					RateID:loginAuth_request_data.RateID, // Rate ID, 1: 64K，2: 128K，3: 256K，4: 2M，5: 8M 
					ResoID:loginAuth_request_data.ResoID, 
					IsLocked:loginAuth_request_data.IsLocked, // Resolution, 1: QCIF (178*144), 2: CIF(352*288), 3: QVGA (320*240), 4: VGA (640*480) Locked status, 1:Locked 0::Unlocked 
					CommunityCode:loginAuth_request_data.CommunityCode, // Community code 
					UserName:loginAuth_request_data.UserName, // User name 
					DefaultLang:loginAuth_request_data.DefaultLang, // Default user metadata display language 
					Definition:loginAuth_request_data.Definition, 
					ServiceCode:loginAuth_request_data.ServiceCode, // User definition, 1: SD; 2: SD-H; 4: HD //Family service switch, Each binary define a switch 0: enabled 1:not enabled Right up the 1st: family DVR properties Right up the 2nd: family Whole Home DVR properties Right up the 3rd: Remote DVR properties Right up the 4th: LookBack properties Right up the 5th: Startover properties 
					CreditLimit:loginAuth_request_data.CreditLimit, 
					bandwidth:loginAuth_request_data.bandwidth, 
					blocktitlelevel:loginAuth_request_data.blocktitlelevel, // User credit limit // bandwidth:15000;sd:2;hd:3 //block title level 
					privatesetting:loginAuth_request_data.privatesetting,
					isUTCTime:loginAuth_request_data.isUTCTime, 
					paymode:loginAuth_request_data.paymode, 
					opensource:loginAuth_request_data.opensource, 
					dtvaccount:loginAuth_request_data.dtvaccount, // DTV Account Number
					};
					break;
				default:
					return null;
					break;
				}
			}
			
		};
		
	}
