/**
 * Getting the UserToken
 */

function getusertoken(ipadd,port,UserID,Authenticator,TerminalFlag) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/mobilegetusertoken.jsp";
	var userToken_request=new XMLHttpRequest();
	var user_token_params={"UserID":UserID,"Authenticator":Authenticator,"TerminalFlag":TerminalFlag};
	userToken_request.open("POST", url,true);
	userToken_request.setRequestHeader("Conten-type","application/json");
	userToken_request.send(JSON.stringify(user_token_params));
	userToken_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			var userToken_request_data=JSON.parse(userToken_request.responseText)
			var responseCode=parseInt(userToken_request_data.ReturnCode);
			switch (responseCode) {
			case 00:
				return {
				ErrorMsg : userToken_request_data.ErrorMsg,
				EPGDomain : userToken_request_data.EPGDomain,
				EPGDomainBackup :userToken_request_data.EPGDomainBackup,
				UpgradeDomain : userToken_request_data.UpgradeDomain,
				UpgradeDomainBackup:userToken_request_data.UpgradeDomainBackup,
				ManagementDomain:userToken_request_data.ManagementDomain,
				ManagementDomainBackup:userToken_request_data.ManagementDomainBackup,
				NTPDomainBackup:userToken_request_data.NTPDomainBackup,
				UserToken:userToken_request_data.UserToken,
				UserTokenExpiredTime:userToken_request_data.UserTokenExpiredTime,
				EPGGroupNMB:userToken_request_data.EPGGroupNMB,
				UserGroupNMB:userToken_request_data.UserGroupNMB,
				TVMSDomain:userToken_request_data.TVMSDomain,
				TVMSDomainBackup:userToken_request_data
						};
				break;
			default:
				return null;
				break;
			}
		}
		
	};
	
}