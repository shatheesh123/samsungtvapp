/**
 * Querying the SubColumn List
 */

function setquerysubcolumn(ipadd,port,pageno,numperpage,columncode,sorttype,isfilter) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getcolumnleaves.jsp?pageno="+pageno+"&numperpage="+numperpage+"&columncode="+columncode+"&sorttype="+sorttype+"&isfilter="+isfilter;
	var querysubcolumn_request= new XMLHttpRequest();
	querysubcolumn_request.open("GET", url, true);
	querysubcolumn_request.send();
	querysubcolumn_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			querysubcolumn_request_data=JSON.parse(querysubcolumn_request.responseText);
			subcolumnCode=parseInt(querysubcolumn_request_data.returncode);
			switch (subcolumnCode) {
			case 00:
				return{
				totalcount:setquerycolumn_request_data.totalcount, // Number of records 
				columncode:setquerycolumn_request_data.columncode, // Column code 
				parentcode:setquerycolumn_request_data.parentcode, // Parent column code 
				columnname:setquerycolumn_request_data.columnname, // Column name 
				description:setquerycolumn_request_data.description, // Column description 
				subexist:setquerycolumn_request_data.subexist, // Sub-column exists or not, 0: No, 1: Yes 
				columntype:setquerycolumn_request_data.columntype, // Column type, 0:VOD program, 2:channel, 8:kalaoke, 26:vas. 
				hasposter:setquerycolumn_request_data.hasposter, 
				customprice:setquerycolumn_request_data.customprice, 
				status:setquerycolumn_request_data.status, // Has poster or not, 0: No, 1: Yes //Column price //Column status ,0: no release, 1: release 
				telecomcode:setquerycolumn_request_data.telecomcode, // External code of the column 
				mediacode:setquerycolumn_request_data.mediacode, // Media provider code 
				bocode:setquerycolumn_request_data.bocode, // Service operator code 
				advertised:setquerycolumn_request_data.advertised, // Support ads or not, 0: No, >0: Collection of media service type. see General Explanatory Notes. 
				normalposter:setquerycolumn_request_data.normalposter, // Normal poster path (relative path) 
				smallposter:setquerycolumn_request_data.smallposter, // Small poster path (relative path)
				bigposter:setquerycolumn_request_data.bigposter, // Big poster path (relative path) 
				iconposter:setquerycolumn_request_data.iconposter, // Icon poster path (relative path) 
				titleposter:setquerycolumn_request_data.titleposter, // Title poster path (relative path) 
				advertisementposter:setquerycolumn_request_data.advertisementposter, // Ads poster path (relative path) 
				sketchposter:setquerycolumn_request_data.sketchposter, // Sketch poster path (relative path) 
				bgposter:setquerycolumn_request_data.bgposter, // Background poster path (relative path) 
				otherposter1:setquerycolumn_request_data.otherposter1, // Other poster path (relative path) 
				otherposter2:setquerycolumn_request_data.otherposter2, // Other poster path (relative path) 
				otherposter3:setquerycolumn_request_data.otherposter3, // Other poster path (relative path) 
				otherposter4:setquerycolumn_request_data.otherposter4, 
				ishidden:setquerycolumn_request_data.ishidden // Other poster path (relative path) //weather to hide Column ,0:no，1:yes "vodcount":["",""], "sortname":["",""], "channelcode":["",""], // Number of programs have been audited in this column. // sortname. // SVOD columns of channel code, else column is null				
			};
				break;

			default:
				return null;
				break;
			}
		}
	};
	
}
