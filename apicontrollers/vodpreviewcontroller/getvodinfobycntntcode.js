/**
 * Querying Detail Information of VOD Program (By Content Code)
 */

function getvodinfobycontentcode(ipadd,port,contentcode,columncode,mediaservices,isquerystarrating){
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getvodinfobycontentcode.jsp?contentcode="+contentcode+"&columncode="+columncode+"&mediaservice="+mediaservice+"&isquerystarrating="+isquerystarrating;
	var getvodinfobycontentcode_request=new XMLHttpRequest();
	getvodinfobycontentcode_request.open("GET",url,true);
	getvodinfobycontentcode_request.send();
	getvodinfobycontentcode_request.onreadystatechange=function(){
	if(this.readyState==4 && this.status==200){
		getvodinfobycontentcode_request_data=JSON.parse(getvodinfobycontentcode_request_data.responseText);
		getvodinfobycontentcodeCode=parseInt(getvodinfobycontentcode_request_data.returncode);
		switch (getvodinfobycontentcodeCode) {
		case 00:
			return{
			totalcount:getvodinfobycode_request_data.totalcount, // Number of records 
			programcode:getvodinfobycode_request_data.programcode, // VOD program code，if it is series, the value is series head code 
			programname:getvodinfobycode_request_data.programname, // VOD program name 
			programnamelen:getvodinfobycode_request_data.programnamelen, // VOD program name length 
			programtype:getvodinfobycode_request_data.programtype, // VOD program type, 1: vod program, 9: Ads, 10: One episode of the series, 14: Series head 
			contentcode:getvodinfobycode_request_data.contentcode, // Content code, series head is series head code. 
			seriesprogramcode:getvodinfobycode_request_data.seriesprogramcode, // Series head program code, Single-episode series program corresponding series head code 
			isrecommend:getvodinfobycode_request_data.isrecommend, // Recommend or not, 0: No, 1: Yes, Note: it is from metadata, it is different from recommend data. 
			ishot:getvodinfobycode_request_data.ishot, // Hot or not, 0: No, 1: Yes, Note: it is from metadata, it is different from hot data. 
			isfirstpage:getvodinfobycode_request_data.isfirstpage, // First page poster or not, 0: No, 1: Yes 
			catagorycode:getvodinfobycode_request_data.catagorycode, // Column code of CP 
			bocode:getvodinfobycode_request_data.bocode, // Service operator code 
			programsearchkey:getvodinfobycode_request_data.programsearchkey, // Program name search keywords (program name initials). 
			mediaservices:getvodinfobycode_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes)
			ratingid:getvodinfobycode_request_data.ratingid, // Rating ID, see General Explanatory Notes. 
			recommendid:getvodinfobycode_request_data.recommendid, // Recommend ID 
			sortnum:getvodinfobycode_request_data.sortnum, // Sort number 
			price:getvodinfobycode_request_data.price, // Price, unit: cent 
			enabledtime:getvodinfobycode_request_data.enabledtime, // Enabled time, format:YYYY.MM.DD HH:24MI:SS 
			disabledtime:getvodinfobycode_request_data.disabledtime, // Disabled time, format:YYYY.MM.DD HH:24MI:SS 
			onlinetime:getvodinfobycode_request_data.onlinetime, // On-line time, format:YYYY.MM.DD HH:24MI:SS 
			offlinetime:getvodinfobycode_request_data.offlinetime, // Off-line time, format:YYYY.MM.DD HH:24MI:SS 
			createtime:getvodinfobycode_request_data.createtime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
			countryname:getvodinfobycode_request_data.countryname, // Country name 
			issimpletrailer:getvodinfobycode_request_data.issimpletrailer, // Simple trailer or not, 0: No, 1: Yes 
			telecomcode:getvodinfobycode_request_data.telecomcode, // External code 
			mediacode:getvodinfobycode_request_data.mediacode, // Media provider code 
			trailerbegintime:getvodinfobycode_request_data.trailerbegintime, // Simple trailer start time, format: HH:MM:SS 
			trailerendtime:getvodinfobycode_request_data.trailerendtime, // Simple trailer end time, format: HH:MM:SS 
			seriesnum:getvodinfobycode_request_data.seriesnum, // Number of episodes of the series, 1 by default for non-series. 
			posterfilelist:getvodinfobycode_request_data.posterfilelist, // Poster name list.This field contains 12 posters separated by ; . Poster arranging order is from IPTV, MVS-Mobile, PC to MVS-Tablets, each screen with three posters including normal poster, smaller poster and big poster. This field is splited into poster1 to poster12 field. 
			posterpath:getvodinfobycode_request_data.posterpath, // Relative path of poster, default is ../images/poster/. 
			wggenre:getvodinfobycode_request_data.wggenre, // Content style type of WenGuang content provider
			wgkeywords:getvodinfobycode_request_data.wgkeywords, // Keywords of WenGuang content provider 
			wgtags:getvodinfobycode_request_data.wgtags, // Tags of WenGuang content provider 
			description:getvodinfobycode_request_data.description, // Program description 
			director:getvodinfobycode_request_data.director, // Director name list, separate by ‘;’ 
			directorsearchkey:getvodinfobycode_request_data.directorsearchkey, // Director name initials list, separate by ‘;’ 
			actor:getvodinfobycode_request_data.actor, // Actor/actress name list, separate by ‘;’ 
			actorsearchkey:getvodinfobycode_request_data.actorsearchkey, // Actor/actress name initial list, separate by ’ ;’ 
			cpcode:getvodinfobycode_request_data.cpcode, // Content provider code 
			cpname:getvodinfobycode_request_data.cpname, // Content provider name 
			columncode:getvodinfobycode_request_data.columncode, // Column code 
			catalogname:getvodinfobycode_request_data.catalogname, // Content type name, such as music 
			descriptionkey:getvodinfobycode_request_data.descriptionkey, // Content description keywords 
			advertisecontent:getvodinfobycode_request_data.advertisecontent, // Support ads or not, 0: No, >0: Collection of media service type. see General Explanatory Notes. 
			releasedate:getvodinfobycode_request_data.releasedate, // Release date 
			writer:getvodinfobycode_request_data.writer, // Writer list, separate by ; 
			audiolang:getvodinfobycode_request_data.audiolang, // Audio language information list, separate by ‘;’ 
			subtitlelang:getvodinfobycode_request_data.subtitlelang, // Subtitle language information list, separate by ‘;’ 
			pubcompany:getvodinfobycode_request_data.pubcompany, // Publish company name 
			detaildescribed:getvodinfobycode_request_data.detaildescribed, // Detail program description information(optional field) 
			seriesseason:getvodinfobycode_request_data.seriesseason, // Number of seasons 
			genre:getvodinfobycode_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action
			licenseperiod:getvodinfobycode_request_data.licenseperiod, // Lisence plate valid period 
			channelcode:getvodinfobycode_request_data.channelcode, // Channel code of the archived program 
			subgenre:getvodinfobycode_request_data.subgenre, // Sub style type of content, such as fantasy and action 
			shortdesc:getvodinfobycode_request_data.shortdesc, // Aspect, a brief plot description 
			shorttitle:getvodinfobycode_request_data.shorttitle, // Subtitle 
			elapsedtime:getvodinfobycode_request_data.elapsedtime, // Length of program, format: HH:MI:SS 
			starlevel:getvodinfobycode_request_data.starlevel, // Star level, value:0-10 
			istimeshift:getvodinfobycode_request_data.istimeshift, // weather support to timeshift 1:yes，0:no 
			isarchivemode:getvodinfobycode_request_data.isarchivemode, 
			isprotection:getvodinfobycode_request_data.isprotection, 
			timeshiftmode:getvodinfobycode_request_data.timeshiftmode, 
			archivemode:getvodinfobycode_request_data.archivemode, 
			copyprotection:getvodinfobycode_request_data.copyprotection, 
			language:getvodinfobycode_request_data.language, 
			format:getvodinfobycode_request_data.format, 
			aspect:getvodinfobycode_request_data.aspect, 
			dolby:getvodinfobycode_request_data.dolby, 
			episodetitle:getvodinfobycode_request_data.episodetitle, 
			parentaladvisory:getvodinfobycode_request_data.parentaladvisory, 
			catalogtype:getvodinfobycode_request_data.catalogtype, 
			seriestype:getvodinfobycode_request_data.seriestype,
			videocode:getvodinfobycode_request_data.videocode, // Video code
			mediaservice:getvodinfobycode_request_data.mediaservice, // Media service type of video. see General Explanatory Notes. 
			videotype:getvodinfobycode_request_data.videotype, // Video type, 28:positive; 20:Senior trailers 
			definition:getvodinfobycode_request_data.definition, // Definition of video, 1:SD, 2: SD-H, 4:HD 
			videomediacode:getvodinfobycode_request_data.videomediacode, // Media code of video 
			videotelecomcode:getvodinfobycode_request_data.videotelecomcode, // External code of video 
			bitrate:getvodinfobycode_request_data.bitrate, // Bitrate of video, the default value is 2000 SD bitrate in kbps. 
			videoelapsedtime:getvodinfobycode_request_data.videoelapsedtime, // Length of video, format: HH:MI:SS 
			encrypttype:getvodinfobycode_request_data.encrypttype, // Video encryption type, 0 :no encryption, o1:self-development encryption, 2:irdeto encryption 
			cntmediacode:getvodinfobycode_request_data.cntmediacode, // Media code of content 
			cnttelecomcode:getvodinfobycode_request_data.cnttelecomcode, 
			ratingnum:getvodinfobycode_request_data.ratingnum,
			ratingsum:getvodinfobycode_request_data.ratingsum,
		};
			break;

		default:
			return null;
			break;
		}
	}	
};
	
	
}