/**
 * Querying VOD Program List
 */

function getvodprogram(ipadd,port,pageno,numperpage,columncode,channelcode,sorttype,mediaservices,fields,isfilterratingid,seriestype) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getvod.jsp?pageno="+pageno+"&numperpage="+numperpage+"&columncode="+columncode+"&channelcode="+channelcode+"&sorttype="+sorttype+"&mediaservices="+mediaservices+"&fields="+fields+"&isfilterratingid="+isfilterratingid+"&seriestype="+seriestype;
	var getvodprogram_request=new XMLHttpRequest();
	getvodprogram_request.open("GET", url, true);
	getvodprogram_request.send();
	getvodprogram_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getvodprogram_request_data=JSON.parse(getvodprogram_request.responseText);
			getgetvodprogramCode=parseInt(getvodprogram_request_data.returncode);
			switch (getgetvodprogramCode) {
			case 00:
				return {
				totalcount:getvodprogram_request_data.totalcount,
				programcode:getvodprogram_request_data.programcode, // VOD program code，if it is series, the value is series head code 
				programname:getvodprogram_request_data.programname, // VOD program name 
				programnamelen:getvodprogram_request_data.programnamelen, // VOD program name length 
				programtype:getvodprogram_request_data.programtype, // VOD program type, 1: vod program, 9: Ads, 10: One episode of the series, 14: Series head 
				contentcode:getvodprogram_request_data.contentcode, // Content code, series head is series head code. 
				seriesprogramcode:getvodprogram_request_data.seriesprogramcode, // Series head program code, Single-episode series program corresponding series head code 
				isrecommend:getvodprogram_request_data.isrecommend, // Recommend or not, 0: No, 1: Yes, Note: it is from metadata, it is different from recommend data. 
				ishot:getvodprogram_request_data.ishot, // Hot or not, 0: No, 1: Yes, Note: it is from metadata, it is different from hot data. 
				isfirstpage:getvodprogram_request_data.isfirstpage, // First page poster or not, 0: No, 1: Yes 
				catagorycode:getvodprogram_request_data.catagorycode, // Column code of CP 
				bocode:getvodprogram_request_data.bocode, // Service operator code 
				programsearchkey:getvodprogram_request_data.programsearchkey, // Program name search keywords (program name initials). 
				mediaservices:getvodprogram_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
				ratingid:getvodprogram_request_data.ratingid, // Rating ID, see General Explanatory Notes. 
				recommendid:getvodprogram_request_data.recommendid, // Recommend ID 
				sortnum:getvodprogram_request_data.sortnum, // Sort number 
				price:getvodprogram_request_data.price, // Price, unit: cent 
				enabledtime:getvodprogram_request_data.enabledtime, // Enabled time, format:YYYY.MM.DD HH:24MI:SS 
				disabledtime:getvodprogram_request_data.disabledtime, // Disabled time, format:YYYY.MM.DD HH:24MI:SS 
				onlinetime:getvodprogram_request_data.onlinetime, // On-line time, format:YYYY.MM.DD HH:24MI:SS 
				offlinetime:getvodprogram_request_data.offlinetime, // Off-line time, format:YYYY.MM.DD HH:24MI:SS 
				createtime:getvodprogram_request_data.createtime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
				countryname:getvodprogram_request_data.countryname, // Country name 
				issimpletrailer:getvodprogram_request_data.issimpletrailer, // Simple trailer or not, 0: No, 1: Yes 
				telecomcode:getvodprogram_request_data.telecomcode, // External code 
				mediacode:getvodprogram_request_data.mediacode, // Media provider code 
				trailerbegintime:getvodprogram_request_data.trailerbegintime, // Simple trailer start time, format: HH:MM:SS 
				trailerendtime:getvodprogram_request_data.trailerendtime, // Simple trailer end time, format: HH:MM:SS 
				seriesnum:getvodprogram_request_data.seriesnum, // Number of episodes of the series, 1 by default for non-series. 
				posterfilelist:getvodprogram_request_data.posterfilelist, 
				posterpath:getvodprogram_request_data.posterpath, // Relative path of poster, default is ../images/poster/. 
				wggenre:getvodprogram_request_data.wggenre, // Content style type of WenGuang content provider 
				wgkeywords:getvodprogram_request_data.wgkeywords, // Keywords of WenGuang content provider
				wgtags:getvodprogram_request_data.wgtags, // Tags of WenGuang content provider 
				description:getvodprogram_request_data.description, // Program description 
				director:getvodprogram_request_data.director, // Director name list, separate by ‘;’ 
				directorsearchkey:getvodprogram_request_data.directorsearchkey, // Director name initials list, separate by ‘;’ 
				actor:getvodprogram_request_data.directorsearchkey, // Actor/actress name list, separate by ‘;’ 
				actorsearchkey:getvodprogram_request_data.actorsearchkey, // Actor/actress name initial list, separate by ’ ;’ 
				cpcode:getvodprogram_request_data.cpcode, // Content provider code 
				cpname:getvodprogram_request_data.cpname, // Content provider name 
				columncode:getvodprogram_request_data.columncode, // Column code 
				catalogname:getvodprogram_request_data.catalogname, // Content type name, such as music 
				descriptionkey:getvodprogram_request_data.descriptionkey, // Content description keywords 
				advertisecontent:getvodprogram_request_data.advertisecontent, // Support ads or not, 0: No, >0: Collection of media service type. see General Explanatory Notes. 
				releasedate:getvodprogram_request_data.releasedate, // Release date 
				writer:getvodprogram_request_data.writer, // Writer list, separate by ; 
				audiolang:getvodprogram_request_data.audiolang, // Audio language information list, separate by ‘;’ 
				subtitlelang:getvodprogram_request_data.subtitlelang, // Subtitle language information list, separate by ‘;’ 
				pubcompany:getvodprogram_request_data.pubcompany, // Publish company name 
				detaildescribed:getvodprogram_request_data.detaildescribed, // Detail program description information(optional field) 
				seriesseason:getvodprogram_request_data.seriesseason, // Number of seasons 
				genre:getvodprogram_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action 
				licenseperiod:getvodprogram_request_data.licenseperiod, // Lisence plate valid period 
				channelcode:getvodprogram_request_data.channelcode, // Channel code of the archived program 
				subgenre:getvodprogram_request_data.subgenre, // Sub style type of content, such as fantasy and action 
				shortdesc:getvodprogram_request_data.shortdesc, // Aspect, a brief plot description 
				shorttitle:getvodprogram_request_data.shorttitle, // Subtitle 
				elapsedtime:getvodprogram_request_data.elapsedtime, // Length of program, format: HH:MI:SS 
				starlevel:getvodprogram_request_data.starlevel, // Star level, value:0-10 
				istimeshift:getvodprogram_request_data.istimeshift, // weather support to timeshift 1:yes，0:no 
				isarchivemode:getvodprogram_request_data.isarchivemode, 
				isprotection:getvodprogram_request_data.isprotection, 
				timeshiftmode:getvodprogram_request_data.timeshiftmode, 
				archivemode:getvodprogram_request_data.archivemode, 
				copyprotection:getvodprogram_request_data.copyprotection, 
				language:getvodprogram_request_data.language, 
				format:getvodprogram_request_data.format, 
				aspect:getvodprogram_request_data.aspect, 
				dolby:getvodprogram_request_data.dolby, 
				episodetitle:getvodprogram_request_data.episodetitle, 
				parentaladvisory:getvodprogram_request_data.parentaladvisory, 
				catalogtype:getvodprogram_request_data.catalogtype, 
				seriestype:getvodprogram_request_data.seriestype,
			};
				break;

			default:
				return null
				break;
			}
		}
		
	};
}