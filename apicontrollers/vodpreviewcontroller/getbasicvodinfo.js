/**
 * Querying Information of VOD Program
 */

function getbasicvodinfo(ipadd,port,programcode,contentcode,columncode,mediaservices,isquerystarrating){
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getbasicvodinfo.jsp?programcode="+programcode+"&contentcode="+contentcode+"&columncode="+columncode+"&mediaservices="+mediaservices+"&isquerystarrating="+isquerystarrating;
	var getbasicvodinfo_request=new XMLHttpRequest();
	getbasicvodinfo_request.open("GET", url,true);
	getbasicvodinfo_request.send();
	getbasicvodinfo_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getbasicvodinfo_request_data=JSON.parse(getbasicvodinfo_request.responseText);
			getbasicvodinfoCode=parseInt(getbasicvodinfo_request_data.returncode);
			switch (getbasicvodinfoCode) {
			case 00:
				return {
				totalcount:getbasicvodinfo_request_data.totalcount,
				programcode:getbasicvodinfo_request_data.programcode, // VOD program code，if it is series, the value is series head code
				programname:getbasicvodinfo_request_data.programname, // VOD program name
				programnamelen:getbasicvodinfo_request_data.programname, // VOD program name length 
				programtype:getbasicvodinfo_request_data.programtype, // VOD program type, 1: vod program, 9: Ads, 10: One episode of the series, 14: Series head 
				contentcode:getbasicvodinfo_request_data.contentcode, // Content code, series head is series head code. 
				seriesprogramcode:getbasicvodinfo_request_data.seriesprogramcode, // Series head program code, Single-episode series program corresponding series head code 
				isrecommend:getbasicvodinfo_request_data.isrecommend, // Recommend or not, 0: No, 1: Yes, Note: it is from metadata, it is different from recommend data. 
				ishot:getbasicvodinfo_request_data.ishot, // Hot or not, 0: No, 1: Yes, Note: it is from metadata, it is different from hot data. 
				isfirstpage:getbasicvodinfo_request_data.isfirstpage, // First page poster or not, 0: No, 1: Yes 
				catagorycode:getbasicvodinfo_request_data.catagorycode, // Column code of CP 
				bocode:getbasicvodinfo_request_data.bocode, // Service operator code 
				programsearchkey:getbasicvodinfo_request_data.programsearchkey, // Program name search keywords (program name initials). 
				mediaservices:getbasicvodinfo_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
				ratingid:getbasicvodinfo_request_data.ratingid, // Rating ID, see General Explanatory Notes. 
				recommendid:getbasicvodinfo_request_data.recommendid, // Recommend ID 
				sortnum:getbasicvodinfo_request_data.sortnum, // Sort number 
				price:getbasicvodinfo_request_data.price, // Price, unit: cent 
				enabledtime:getbasicvodinfo_request_data.enabledtime, // Enabled time, format:YYYY.MM.DD HH:24MI:SS 
				disabledtime:getbasicvodinfo_request_data.disabledtime, // Disabled time, format:YYYY.MM.DD HH:24MI:SS 
				onlinetime:getbasicvodinfo_request_data.onlinetime, // On-line time, format:YYYY.MM.DD HH:24MI:SS 
				offlinetime:getbasicvodinfo_request_data.offlinetime, // Off-line time, format:YYYY.MM.DD HH:24MI:SS 
				createtime:getbasicvodinfo_request_data.createtime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
				countryname:getbasicvodinfo_request_data.countryname, // Country name 
				issimpletrailer:getbasicvodinfo_request_data.issimpletrailer, // Simple trailer or not, 0: No, 1: Yes 
				telecomcode:getbasicvodinfo_request_data.telecomcode, // External code 
				mediacode:getbasicvodinfo_request_data.mediacode, // Media provider code 
				trailerbegintime:getbasicvodinfo_request_data.trailerbegintime, // Simple trailer start time, format: HH:MM:SS 
				trailerendtime:getbasicvodinfo_request_data.trailerendtime, // Simple trailer end time, format: HH:MM:SS 
				seriesnum:getbasicvodinfo_request_data.seriesnum, // Number of episodes of the series, 1 by default for non-series. 
				posterfilelist:getbasicvodinfo_request_data.posterfilelist, // Poster name list.This field contains 12 posters separated by ; . Poster arranging order is from IPTV, MVS-Mobile, PC to MVS-Tablets, each screen with three posters including normal poster, smaller poster and big poster. This field is splited into poster1 to poster12 field. 
				posterpath:getbasicvodinfo_request_data.posterpath, // Relative path of poster, default is ../images/poster/. 
				wggenre:getbasicvodinfo_request_data.wggenre, // Content style type of WenGuang content provider 
				wgkeywords:getbasicvodinfo_request_data.wgkeywords, // Keywords of WenGuang content provider
				wgtags:getbasicvodinfo_request_data.wgtags, // Tags of WenGuang content provider 
				description:getbasicvodinfo_request_data.description, // Program description 
				director:getbasicvodinfo_request_data.director, // Director name list, separate by ‘;’ 
				directorsearchkey:getbasicvodinfo_request_data.directorsearchkey, // Director name initials list, separate by ‘;’ 
				actor:getbasicvodinfo_request_data.actor, // Actor/actress name list, separate by ‘;’ 
				actorsearchkey:getbasicvodinfo_request_data.actorsearchkey, // Actor/actress name initial list, separate by ’ ;’ 
				cpcode:getbasicvodinfo_request_data.cpcode, // Content provider code 
				cpname:getbasicvodinfo_request_data.cpname, // Content provider name 
				columncode:getbasicvodinfo_request_data.columncode, // Column code 
				catalogname:getbasicvodinfo_request_data.catalogname, // Content type name, such as music 
				descriptionkey:getbasicvodinfo_request_data.descriptionkey, // Content description keywords 
				advertisecontent:getbasicvodinfo_request_data.advertisecontent, // Support ads or not, 0: No, >0: Collection of media service type. see General Explanatory Notes. 
				releasedate:getbasicvodinfo_request_data.releasedate, // Release date 
				writer:getbasicvodinfo_request_data.writer, // Writer list, separate by ; 
				audiolang:getbasicvodinfo_request_data.audiolang, // Audio language information list, separate by ‘;’ 
				subtitlelang:getbasicvodinfo_request_data.subtitlelang, // Subtitle language information list, separate by ‘;’ 
				pubcompany:getbasicvodinfo_request_data.pubcompany, // Publish company name 
				detaildescribed:getbasicvodinfo_request_data.detaildescribed, // Detail program description information(optional field) 
				seriesseason:getbasicvodinfo_request_data.seriesseason, // Number of seasons 
				genre:getbasicvodinfo_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action 
				licenseperiod:getbasicvodinfo_request_data.licenseperiod, // Lisence plate valid period 
				channelcode:getbasicvodinfo_request_data.channelcode, // Channel code of the archived program 
				subgenre:getbasicvodinfo_request_data.subgenre, // Sub style type of content, such as fantasy and action 
				shortdesc:getbasicvodinfo_request_data.shortdesc, // Aspect, a brief plot description 
				shorttitle:getbasicvodinfo_request_data.shorttitle, // Subtitle 
				elapsedtime:getbasicvodinfo_request_data.elapsedtime, // Length of program, format: HH:MI:SS 
				starlevel:getbasicvodinfo_request_data.starlevel, // Star level, value:0-10 
				istimeshift:getbasicvodinfo_request_data.istimeshift, // weather support to timeshift 1:yes，0:no 
				isarchivemode:getbasicvodinfo_request_data.isarchivemode,
				isprotection:getbasicvodinfo_request_data.isprotection,
				timeshiftmode:getbasicvodinfo_request_data.timeshiftmode,
				archivemode:getbasicvodinfo_request_data.archivemode,
				copyprotection:getbasicvodinfo_request_data.copyprotection,
				language:getbasicvodinfo_request_data.language,
				format:getbasicvodinfo_request_data.format,
				aspect:getbasicvodinfo_request_data.aspect,
				dolby:getbasicvodinfo_request_data.dolby, 
				episodetitle:getbasicvodinfo_request_data.episodetitle,
				parentaladvisory:getbasicvodinfo_request_data.parentaladvisory, 
				catalogtype:getbasicvodinfo_request_data.catalogtype, 
				seriestype:getbasicvodinfo_request_data.seriestype,
			};
				
				break;

			default:
				break;
			}
		}
	};
	
}