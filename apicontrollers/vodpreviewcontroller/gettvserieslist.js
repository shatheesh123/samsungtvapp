/**
 * Querying TV Series List
 */
function gettvserieslist(ipadd,port,pageno,numperpage,columncode,sorttype,mediaservices,isfilterratingid,seriestype) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getserieshead.jsp?pageno="+pageno+"&numperpage="+numperpage+"&columncode="+columncode+"&sorttype="+sorttype+"&mediaservices="+mediaservices+"&isfilterratingid="+isfilterratingid+"&seriestype="+seriestype;
	var tvserieslist_request=new XMLHttpRequest();
	tvserieslist_request.open("GET", url, true);
	tvserieslist_request.send();
	tvserieslist_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			tvserieslist_request_data=JSON.parse(tvserieslist_request.responseText);
			tvserieslistCode=parseInt(tvserieslist_request_data.returncode);
			switch (tvserieslistCode) {
			case 00:
				return{
				totalcount:tvserieslist_request_data.totalcount,
				programcode:tvserieslist_request_data.programcode, // VOD program code，if it is series, the value is series head code 
				programname:tvserieslist_request_data.programname, // VOD program name 
				programnamelen:tvserieslist_request_data.programnamelen, // VOD program name length 
				programtype:tvserieslist_request_data.programtype, // VOD program type, 1: vod program, 9: Ads, 10: One episode of the series, 14: Series head 
				contentcode:tvserieslist_request_data.contentcode, // Content code, series head is series head code. 
				seriesprogramcode:tvserieslist_request_data.seriesprogramcode, // Series head program code, Single-episode series program corresponding series head code 
				isrecommend:tvserieslist_request_data.isrecommend, // Recommend or not, 0: No, 1: Yes, Note: it is from metadata, it is different from recommend data.
				ishot:tvserieslist_request_data.ishot, // Hot or not, 0: No, 1: Yes, Note: it is from metadata, it is different from hot data.
				isfirstpage:tvserieslist_request_data.isfirstpage, // First page poster or not, 0: No, 1: Yes 
				catagorycode:tvserieslist_request_data.catagorycode, // Column code of CP 
				bocode:tvserieslist_request_data.bocode, // Service operator code 
				programsearchkey:tvserieslist_request_data.programsearchkey, // Program name search keywords (program name initials). 
				mediaservices:tvserieslist_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
				ratingid:tvserieslist_request_data.ratingid, // Rating ID, see General Explanatory Notes. 
				recommendid:tvserieslist_request_data.recommendid, // Recommend ID 
				sortnum:tvserieslist_request_data.sortnum, // Sort number 
				price:tvserieslist_request_data.price, // Price, unit: cent 
				enabledtime:tvserieslist_request_data.enabledtime, // Enabled time, format:YYYY.MM.DD HH:24MI:SS 
				disabledtime:tvserieslist_request_data.disabledtime, // Disabled time, format:YYYY.MM.DD HH:24MI:SS 
				onlinetime:tvserieslist_request_data.onlinetime, // On-line time, format:YYYY.MM.DD HH:24MI:SS 
				offlinetime:tvserieslist_request_data.offlinetime, // Off-line time, format:YYYY.MM.DD HH:24MI:SS 
				createtime:tvserieslist_request_data.createtime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
				countryname:tvserieslist_request_data.countryname, // Country name 
				issimpletrailer:tvserieslist_request_data.issimpletrailer, // Simple trailer or not, 0: No, 1: Yes 
				telecomcode:tvserieslist_request_data.telecomcode, // External code 
				mediacode:tvserieslist_request_data.mediacode, // Media provider code 
				trailerbegintime:tvserieslist_request_data.trailerbegintime, // Simple trailer start time, format: HH:MM:SS 
				trailerendtime:tvserieslist_request_data.trailerendtime, // Simple trailer end time, format: HH:MM:SS 
				seriesnum:tvserieslist_request_data.seriesnum, // Number of episodes of the series, 1 by default for non-series. 
				posterfilelist:tvserieslist_request_data.posterfilelist, // Poster name list.This field contains 12 posters separated by ; . Poster arranging order is from IPTV, MVS-Mobile, PC to MVS-Tablets, each screen with three posters including normal poster, smaller poster and big poster. This field is splited into poster1 to poster12 field. 
				posterpath:tvserieslist_request_data.posterpath, // Relative path of poster, default is ../images/poster/. 
				wggenre:tvserieslist_request_data.wggenre, // Content style type of WenGuang content provider 
				wgkeywords:tvserieslist_request_data.wgkeywords, // Keywords of WenGuang content provider
				wgtags:tvserieslist_request_data.wgtags, // Tags of WenGuang content provider 
				description:tvserieslist_request_data.description, // Program description 
				director:tvserieslist_request_data.director, // Director name list, separate by ‘;’ 
				directorsearchkey:tvserieslist_request_data.directorsearchkey, // Director name initials list, separate by ‘;’ 
				actor:tvserieslist_request_data.actor, // Actor/actress name list, separate by ‘;’ 
				actorsearchkey:tvserieslist_request_data.actorsearchkey, // Actor/actress name initial list, separate by ’ ;’ 
				cpcode:tvserieslist_request_data.cpcode, // Content provider code 
				cpname:tvserieslist_request_data.cpname, // Content provider name 
				columncode:tvserieslist_request_data.columncode, // Column code 
				catalogname:tvserieslist_request_data.catalogname, // Content type name, such as music 
				descriptionkey:tvserieslist_request_data.descriptionkey, // Content description keywords 
				advertisecontent:tvserieslist_request_data.advertisecontent, // Support ads or not, 0: No, >0: Collection of media service type. see General Explanatory Notes. 
				releasedate:tvserieslist_request_data.releasedate, // Release date 
				writer:tvserieslist_request_data.writer, // Writer list, separate by ; 
				audiolang:tvserieslist_request_data.audiolang, // Audio language information list, separate by ‘;’ 
				subtitlelang:tvserieslist_request_data.subtitlelang, // Subtitle language information list, separate by ‘;’ 
				pubcompany:tvserieslist_request_data.pubcompany, // Publish company name 
				detaildescribed:tvserieslist_request_data.detaildescribed, // Detail program description information(optional field) 
				seriesseason:tvserieslist_request_data.seriesseason, // Number of seasons 
				genre:tvserieslist_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action 
				licenseperiod:tvserieslist_request_data.licenseperiod, // Lisence plate valid period 
				channelcode:tvserieslist_request_data.channelcode, // Channel code of the archived program 
				subgenre:tvserieslist_request_data.subgenre, // Sub style type of content, such as fantasy and action 
				shortdesc:tvserieslist_request_data.shortdesc, // Aspect, a brief plot description 
				shorttitle:tvserieslist_request_data.shorttitle, // Subtitle 
				elapsedtime:tvserieslist_request_data.elapsedtime, // Length of program, format: HH:MI:SS 
				starlevel:tvserieslist_request_data.starlevel, // Star level, value:0-10 
				istimeshift:tvserieslist_request_data.istimeshift, // weather support to timeshift 1:yes，0:no 
				isarchivemode:tvserieslist_request_data.isarchivemode,
				isprotection:tvserieslist_request_data.isprotection, 
				timeshiftmode:tvserieslist_request_data.timeshiftmode,
				archivemode:tvserieslist_request_data.archivemode,
				copyprotection:tvserieslist_request_data.copyprotection, 
				language:tvserieslist_request_data.language, 
				format:tvserieslist_request_data.format, 
				aspect:tvserieslist_request_data.aspect, 
				dolby:tvserieslist_request_data.dolby, 
				episodetitle:tvserieslist_request_data.episodetitle, 
				parentaladvisory:tvserieslist_request_data.parentaladvisory,
				catalogtype:tvserieslist_request_data.catalogtype, 
				seriestype:tvserieslist_request_data.seriestype, 
			};
				break;

			default:
				break;
			}
		}
	};
}
