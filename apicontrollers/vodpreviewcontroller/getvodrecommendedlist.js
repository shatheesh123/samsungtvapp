/**
 * Querying the Recommended Program List
 */

function getvodrecommendedlist(ipadd,port,pageno,numperpage,numperpage,mediaservices,isfilterratingid,seriestype) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getvodrecommend.jsp?pageno="+pageno+"&numperpage="+numperpage+"&numperpage="+numperpage+"&mediaservices="+mediaservices+"&isfilterratingid="+isfilterratingid+"&seriestype="+seriestype;
	var getvodrecommended_request=new XMLHttpRequest();
	getvodrecommended_request.open("GET", url, true);
	getvodrecommended_request.send();
	getvodrecommended_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getvodrecommended_request_data=JSON.parse(getvodrecommended_request.responseText);
			getvodrecommendedCode=parseInt(getvodrecommended_request_data.returncode);
			switch (getvodrecommendedCode) {
			case 00:
				return {
				totalcount : vodsearch_request_data.totalcount,
				programcode: vodsearch_request_data.programcode, // VOD program code，if it is series, the value is series head code 
				programname:vodsearch_request_data.programname, // VOD program name 
				programnamelen:vodsearch_request_data.programnamelen, // VOD program name length 
				programtype:vodsearch_request_data.programtype, // VOD program type, 1: vod program, 9: Ads, 10: One episode of the series, 14: Series head 
				contentcode:vodsearch_request_data.contentcode, // Content code, series head is series head code. 
				seriesprogramcode:vodsearch_request_data.seriesprogramcode, // Series head program code, Single-episode series program corresponding series head code 
				isrecommend:vodsearch_request_data.isrecommend, // Recommend or not, 0: No, 1: Yes, Note: it is from metadata, it is different from recommend data. 
				ishot:vodsearch_request_data.ishot, // Hot or not, 0: No, 1: Yes, Note: it is from metadata, it is different from hot data. 
				isfirstpage:vodsearch_request_data.isfirstpage, // First page poster or not, 0: No, 1: Yes 
				catagorycode:vodsearch_request_data.catagorycode, // Column code of CP 
				bocode:vodsearch_request_data.bocode, // Service operator code 
				programsearchkey:vodsearch_request_data.programsearchkey, // Program name search keywords (program name initials). 
				mediaservices:vodsearch_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
				ratingid:vodsearch_request_data.ratingid, // Rating ID, see General Explanatory Notes. 
				recommendid:vodsearch_request_data.recommendid, // Recommend ID 
				sortnum:vodsearch_request_data.sortnum, // Sort number 
				price:vodsearch_request_data.price, // Price, unit: cent 
				enabledtime:vodsearch_request_data.enabledtime, // Enabled time, format:YYYY.MM.DD HH:24MI:SS 
				disabledtime:vodsearch_request_data.disabledtime, // Disabled time, format:YYYY.MM.DD HH:24MI:SS 
				onlinetime:vodsearch_request_data.onlinetime, // On-line time, format:YYYY.MM.DD HH:24MI:SS 
				offlinetime:vodsearch_request_data.offlinetime, // Off-line time, format:YYYY.MM.DD HH:24MI:SS 
				createtime:vodsearch_request_data.createtime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
				countryname:vodsearch_request_data.countryname, // Country name 
				issimpletrailer:vodsearch_request_data.issimpletrailer, // Simple trailer or not, 0: No, 1: Yes 
				telecomcode:vodsearch_request_data.telecomcode, // External code 
				mediacode:vodsearch_request_data.mediacode, // Media provider code 
				trailerbegintime:vodsearch_request_data.trailerbegintime, // Simple trailer start time, format: HH:MM:SS 
				trailerendtime:vodsearch_request_data.trailerendtime, // Simple trailer end time, format: HH:MM:SS 
				seriesnum:vodsearch_request_data.seriesnum, // Number of episodes of the series, 1 by default for non-series. 
				posterfilelist:vodsearch_request_data.posterfilelist, // Poster name list.This field contains 12 posters separated by ; . Poster arranging order is from IPTV, MVS-Mobile, PC to MVS-Tablets, each screen with three posters including normal poster, smaller poster and big poster. This field is splited into poster1 to poster12 field. 
				posterpath:vodsearch_request_data.posterpath, // Relative path of poster, default is ../images/poster/. 
				wggenre:vodsearch_request_data.wggenre, // Content style type of WenGuang content provider 
				wgkeywords:vodsearch_request_data.wgkeywords, // Keywords of WenGuang content provider
				wgtags:vodsearch_request_data.wgtags, // Tags of WenGuang content provider
				description:vodsearch_request_data.description, // Program description 
				director:vodsearch_request_data.director, // Director name list, separate by ‘;’ 
				directorsearchkey:vodsearch_request_data.directorsearchkey, // Director name initials list, separate by ‘;’ 
				actor:vodsearch_request_data.actor, // Actor/actress name list, separate by ‘;’ 
				actorsearchkey:vodsearch_request_data.actorsearchkey, // Actor/actress name initial list, separate by ’ ;’ 
				cpcode:vodsearch_request_data.cpcode, // Content provider code 
				cpname:vodsearch_request_data.cpname, // Content provider name 
				columncode:vodsearch_request_data.columncode, // Column code 
				catalogname:vodsearch_request_data.catalogname, // Content type name, such as music 
				descriptionkey:vodsearch_request_data.descriptionkey, // Content description keywords 
				advertisecontent:vodsearch_request_data.advertisecontent, // Support ads or not, 0: No, >0: Collection of media service type. see General Explanatory Notes. 
				releasedate:vodsearch_request_data.releasedate, // Release date 
				writer:vodsearch_request_data.writer, // Writer list, separate by ; 
				audiolang:vodsearch_request_data.audiolang, // Audio language information list, separate by ‘;’ 
				subtitlelang:vodsearch_request_data.subtitlelang, // Subtitle language information list, separate by ‘;’ 
				pubcompany:vodsearch_request_data.pubcompany, // Publish company name 
				detaildescribed:vodsearch_request_data.detaildescribed, // Detail program description information(optional field) 
				seriesseason:vodsearch_request_data.seriesseason, // Number of seasons 
				genre:vodsearch_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action 
				licenseperiod:vodsearch_request_data.licenseperiod, // Lisence plate valid period 
				channelcode:vodsearch_request_data.channelcode, // Channel code of the archived program 
				subgenre:vodsearch_request_data.subgenre, // Sub style type of content, such as fantasy and action 
				shortdesc:vodsearch_request_data.shortdesc, // Aspect, a brief plot description 
				shorttitle:vodsearch_request_data.shorttitle, // Subtitle 
				elapsedtime:vodsearch_request_data.elapsedtime, // Length of program, format: HH:MI:SS 
				starlevel:vodsearch_request_data.starlevel, // Star level, value:0-10 
				istimeshift:vodsearch_request_data.istimeshift, // weather support to timeshift 1:yes，0:no 
				isarchivemode:vodsearch_request_data.isarchivemode, 
				isprotection:vodsearch_request_data.isprotection, 
				timeshiftmode:vodsearch_request_data.timeshiftmode, 
				archivemode:vodsearch_request_data.archivemode, 
				copyprotection:vodsearch_request_data.copyprotection, 
				language:vodsearch_request_data.language,
				format:vodsearch_request_data.format, 
				aspect:vodsearch_request_data.aspect, 
				dolby:vodsearch_request_data.dolby,
				episodetitle:vodsearch_request_data.episodetitle, 
				parentaladvisory:vodsearch_request_data.parentaladvisory,
				catalogtype:vodsearch_request_data.catalogtype,
				seriestype:vodsearch_request_data.seriestype
			};
				break;

			default:
				return null;
				break;
			}
		}
	};
}