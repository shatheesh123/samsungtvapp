/**
 * Querying the Detail Information of Next Episode of a series
 */

function getnextepisode(ipadd,port,columncode,seriesprogramcode,seriesnum,mediaservices) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getpreseries.jsp?columncode="+columncode+"&seriesprogramcode="+seriesprogramcode+"&seriesnum="+seriesnum+"&mediaservices="+mediaservices;
	var nextepisode_request=new XMLHttpRequest();
	nextepisode_request.open("GET", url,true);
	nextepisode_request.send();
	nextepisode_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			nextepisode_request_data=JSON.parse(nextepisode_request.responseText);
			nextepisodeCode=parseInt(nextepisode_request_data.returncode);
			switch (nextepisodeCode) {
			case 00:
				return{
				totalcount:previousepisode_request_data.totalcount, // Number of records 
				programcode:previousepisode_request_data.programcode, // VOD program code，if it is series, the value is series head code 
				programname:previousepisode_request_data.programname, // VOD program name 
				programnamelen:previousepisode_request_data.programnamelen, // VOD program name length 
				programtype:previousepisode_request_data.programtype, // VOD program type, 1: vod program, 9: Ads, 10: One episode of the series, 14: Series head 
				contentcode:previousepisode_request_data.contentcode, // Content code, series head is series head code. 
				seriesprogramcode:previousepisode_request_data.seriesprogramcode, // Series head program code, Single-episode series program corresponding series head code 
				isrecommend:previousepisode_request_data.isrecommend, // Recommend or not, 0: No, 1: Yes, Note: it is from metadata, it is different from recommend data. 
				ishot:previousepisode_request_data.ishot, // Hot or not, 0: No, 1: Yes, Note: it is from metadata, it is different from hot data. 
				isfirstpage:previousepisode_request_data.isfirstpage, // First page poster or not, 0: No, 1: Yes 
				catagorycode:previousepisode_request_data.catagorycode, // Column code of CP 
				bocode:previousepisode_request_data.bocode, // Service operator code 
				programsearchkey:previousepisode_request_data.programsearchkey, // Program name search keywords (program name initials).
				mediaservices:previousepisode_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see
				ratingid:previousepisode_request_data.ratingid, // Rating ID, see General Explanatory Notes. 
				recommendid:previousepisode_request_data.recommendid, // Recommend ID 
				sortnum:previousepisode_request_data.sortnum, // Sort number 
				price:previousepisode_request_data.price, // Price, unit: cent 
				enabledtime:previousepisode_request_data.enabledtime, // Enabled time, format:YYYY.MM.DD HH:24MI:SS 
				disabledtime:previousepisode_request_data.disabledtime, // Disabled time, format:YYYY.MM.DD HH:24MI:SS 
				onlinetime:previousepisode_request_data.onlinetime, // On-line time, format:YYYY.MM.DD HH:24MI:SS 
				offlinetime:previousepisode_request_data.offlinetime, // Off-line time, format:YYYY.MM.DD HH:24MI:SS 
				createtime:previousepisode_request_data.createtime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
				countryname:previousepisode_request_data.countryname, // Country name 
				issimpletrailer:previousepisode_request_data.issimpletrailer, // Simple trailer or not, 0: No, 1: Yes 
				telecomcode:previousepisode_request_data.telecomcode, // External code 
				mediacode:previousepisode_request_data.mediacode, // Media provider code 
				trailerbegintime:previousepisode_request_data.trailerbegintime, // Simple trailer start time, format: HH:MM:SS 
				trailerendtime:previousepisode_request_data.trailerendtime, // Simple trailer end time, format: HH:MM:SS 
				seriesnum:previousepisode_request_data.seriesnum, // Number of episodes of the series, 1 by default for non-series. 
				posterfilelist:previousepisode_request_data.posterfilelist, // Poster name list.This field contains 12 posters separated by ; . Poster arranging order is from IPTV, MVS-Mobile, PC to MVS-Tablets, each screen with three posters including normal poster, smaller poster and big poster. This field is splited into poster1 to poster12 field. 
				posterpath:previousepisode_request_data.posterpath, // Relative path of poster, default is ../images/poster/. 
				wggenre:previousepisode_request_data.wggenre, // Content style type of WenGuang content provider 
				wgkeywords:previousepisode_request_data.wgkeywords, // Keywords of WenGuang content provider 
				wgtags:previousepisode_request_data.wgtags, // Tags of WenGuang content provider 
				description:previousepisode_request_data.description, // Program description 
				director:previousepisode_request_data.director, // Director name list, separate by ‘;’ 
				directorsearchkey:previousepisode_request_data.directorsearchkey, // Director name initials list, separate by ‘;’ 
				actor:previousepisode_request_data.actor, // Actor/actress name list, separate by ‘;’ 
				actorsearchkey:previousepisode_request_data.actorsearchkey, // Actor/actress name initial list, separate by ’ ;’ 
				cpcode:previousepisode_request_data.cpcode, // Content provider code 
				cpname:previousepisode_request_data.cpname, // Content provider name 
				columncode:previousepisode_request_data.columncode, // Column code 
				catalogname:previousepisode_request_data.catalogname, // Content type name, such as music 
				descriptionkey:previousepisode_request_data.descriptionkey, // Content description keywords 
				advertisecontent:previousepisode_request_data.advertisecontent, // Support ads or not, 0: No, >0: Collection of media service type. see General Explanatory Notes. 
				releasedate:previousepisode_request_data.releasedate, // Release date 
				writer:previousepisode_request_data.writer, // Writer list, separate by ; 
				audiolang:previousepisode_request_data.audiolang, // Audio language information list, separate by ‘;’ 
				subtitlelang:previousepisode_request_data.subtitlelang, // Subtitle language information list, separate by ‘;’ 
				pubcompany:previousepisode_request_data.pubcompany, // Publish company name 
				detaildescribed:previousepisode_request_data.detaildescribed, // Detail program description information(optional field) 
				seriesseason:previousepisode_request_data.seriesseason, // Number of seasons 
				genre:previousepisode_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action
				licenseperiod:previousepisode_request_data.licenseperiod, // Lisence plate valid period 
				channelcode:previousepisode_request_data.channelcode, // Channel code of the archived program 
				subgenre:previousepisode_request_data.subgenre, // Sub style type of content, such as fantasy and action 
				shortdesc:previousepisode_request_data.shortdesc, // Aspect, a brief plot description 
				shorttitle:previousepisode_request_data.shorttitle, // Subtitle 
				elapsedtime:previousepisode_request_data.elapsedtime, // Length of program, format: HH:MI:SS 
				starlevel:previousepisode_request_data.starlevel.starlevel, // Star level, value:0-10 
				istimeshift:previousepisode_request_data.istimeshift, // weather support to timeshift 1:yes，0:no 
				isarchivemode:previousepisode_request_data.isarchivemode, 
				isprotection:previousepisode_request_data.isprotection, 
				timeshiftmode:previousepisode_request_data.timeshiftmode, 
				archivemode:previousepisode_request_data.archivemode,
				copyprotection:previousepisode_request_data.copyprotection, 
				language:previousepisode_request_data.language, 
				format:previousepisode_request_data.format, 
				aspect:previousepisode_request_data.aspect, 
				dolby:previousepisode_request_data.dolby, 
				episodetitle:previousepisode_request_data.episodetitle, 
				parentaladvisory:previousepisode_request_data.parentaladvisory, 
				catalogtype:previousepisode_request_data.catalogtype, 
				seriestype:previousepisode_request_data.seriestype,
				definition:previousepisode_request_data.definition, // Definition of video, 1:SD, 2: SD-H, 4:HD 
				videomediacode:previousepisode_request_data.videomediacode, // Media code of video 
				videotelecomcode:previousepisode_request_data.bitrate, // External code of video 
				bitrate:previousepisode_request_data.bitrate, // Bitrate of video, the default value is 2000 SD bitrate in kbps. 
				videoelapsedtime:previousepisode_request_data.videoelapsedtime, // Length of video, format: HH:MI:SS 
				encrypttype:previousepisode_request_data.encrypttype, // Video encryption type, 0 :no encryption, o1:self-development encryption, 2:irdeto encryption 
				cntmediacode:previousepisode_request_data.cntmediacode, // Media code of content 
				cnttelecomcode:previousepisode_request_data.cnttelecomcode, 
				ratingnum:previousepisode_request_data.ratingnum, 
				ratingsum:previousepisode_request_data.ratingsum, 
			};
				break;
			default:
				return null;
				break;
			}
		}
	};
	
}
	
}