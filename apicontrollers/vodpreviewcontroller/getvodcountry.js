/**
 * Querying the Country List
 */

function getvodcountrylist(ipadd,port) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getcountrylist.jsp";
	var getvodcountrylist_request=new XMLHttpRequest();
	getvodcountrylist_request.open("GET", url, true);
	getvodcountrylist_request.send();
	getvodcountrylist_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getvodcountrylist_request_data=JSON.parse(getvodcountrylist_request.responseText);
			getvodcountrylistCode=parseInt(getvodcountrylist_request_data.returncode);
			switch (getvodcountrylistCode) {
			case 00:
				return{
				totalcount:getvodcountrylist_request_data.totalcount,
				countryname:getvodcountrylist_request_data.countryname,
				countryid:getvodcountrylist_request_data.countryid,
				countrycode:getvodcountrylist_request_data.countrycode
			};
				break;

			default:
				return null;
				break;
			}
		}
	};
}