/**
 * Querying all Information of VOD Program
 */

function getvodinfo(ipadd,port,columncode,programcode,segmenttype,authidsession,mediaservices,isquerystarrating) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getvodinfo.jsp?columncode="+columncode+"&programcode="+programcode+"&segmenttype="+segmenttype+"&authidsession="+authidsession+"&mediaservices="+mediaservices+"&isquerystarrating="+isquerystarrating;
	var getvodinfo_request=new XMLHttpRequest();
	getvodinfo_request.open("GET", url, true);
	getvodinfo_request.send();
	getvodinfo_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getvodinfo_request_data=JSON.parse(getvodinfo_request.responseText);
			return{
				//omitted fields-getbasicvodinfo
				
				bp:getvodinfo_request_data.bp, // Breakpoint information （series：watch set ，VOD：watch time unit：s） 
				url:getvodinfo_request_data.url, // VOD play URL \
				prevueurl:getvodinfo_request_data.prevueurl, // VOD segment play URL 
				snapurl:getvodinfo_request_data.snapurl, //snap play URL 
				clipurl:getvodinfo_request_data.clipurl, //clip play URL
			};
		}
		
	};
	
}