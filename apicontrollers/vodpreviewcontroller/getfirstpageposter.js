/**
 * Querying First Page Poster
 */

function getfirstpage(ipadd,port,framecode,positioncode,type) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getfirstpage.jsp?framecode="+framecode+"&positioncode="+positioncode+"&type="+type;
	var getfirstpage_request=new XMLHttpRequest();
	getfirstpage_request.open("GET", url, true);
	getfirstpage_request.send();
	getfirstpage_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getfirstpage_request_data=JSON.parse(getfirstpage_request.responseText);
			getfirstpageCode=parseInt(getfirstpage_request_data.returncode);
			switch (getfirstpageCode) {
			case 00:
				return{
				totalcount:getfirstpage_request_data.totalcount,
				pageid:getfirstpage_request_data.pageid, // Page ID 
				positioncode:getfirstpage_request_data.positioncode, // Poster postion code 
				framepath:getfirstpage_request_data.framepath, // Frame path, such as framexxx 
				bocode:getfirstpage_request_data.bocode, // Service operator code 
				begintime:getfirstpage_request_data.begintime,
				endtime:getfirstpage_request_data.endtime, // End time has two formats: the schedule poster format: yesYYY.MM.DD HH:24MI:SS, the default poster format: HH:24MI:SS. 
				displaytype:getfirstpage_request_data.displaytype, // Display type, 1: Rolling poster, 2: Rolling text, 3: Designated VOD program, 4: VOD preview, 5: Column link 
				firstpagecode:getfirstpage_request_data.firstpagecode, // Mapping poster code, input poster code, channel code, program code, column code according to different display type. 
				firstpagefile:getfirstpage_request_data.firstpagefile, // poster file name, poster file name and other information(relative path is ../images/firstpageposter/). If there is no poster, it is the default value. 
				contextid:getfirstpage_request_data.contextid, // Association ID, when displaytype=1,7,8, input the mixed channel id, special program id and column id, and use default value for others. 
				contexturl:getfirstpage_request_data.contexturl, // Association url, when the displaytype=7,8, input the special topic indexurl, special column indexurl, and use default value for others. 
				configinfo:getfirstpage_request_data.configinfo,
				synctimestamp:getfirstpage_request_data.synctimestamp
			};
				break;

			default:
				return null;
				break;
			}
		}
	};
	
}