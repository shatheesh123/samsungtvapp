/**
 * Querying Top Play of VOD Program
 */

function getvodhot(ipadd,port,pageno,numperpage,columncode,vodtype,sorttype,mediaservices,isfilterratingid,periodtype) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getvodhot.jsp?pageno="+pageno+"&numperpage="+numperpage+"&columncode="+columncode+"&vodtype="+vodtype+"&sorttype="+sorttype+"&mediaservices="+mediaservices+"&isfilterratingid="+isfilterratingid+"&periodtype="+periodtype;
	var getvodhot_request=new XMLHttpRequest();
	getvodhot_request.open("GET", url, true);
	getvodhot_request.send();
	getvodhot_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			getvodhot_request_data=JSON.parse(getvodhot_request.responseText);
			getvodhotCode=parseInt(getvodhot_request_data.returncode);
			return{
				//omitted fields-getbasicvodinfo
				playnum:getvodhot_request_data.playnum
			};
		}
	}
}