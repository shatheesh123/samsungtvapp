/**
 * Querying the Episode List of a TV Series
 */

function getepisodeslist(ipadd,port,pageno,numperpage,columncode,seriesprogramcode,mediaservices,isfilterratingid,seriestyp){
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getserieschild.jsp?pageno="+pageno+"&numperpage="+numperpage+"&columncode="+columncode+"&seriesprogramcode="+seriesprogramcode+"&mediaservice="+mediaservice+"&isfilterratingid="+isfilterratingid+"&seriestyp="+seriestyp;
	var getepisodeslist_request=new XMLHttpRequest();
	getepisodeslist_request.open("GET", url, true);
	getepisodeslist_request.send();
	getepisodeslist_request.onreadystatechange=function(){
		if(getepisodeslist_request.readyState==4 && getepisodeslist_request.status==200){
			var getepisodeslist_request_data=JSON.parse(getepisodeslist_request.responseText);
			getepisodeslistCode=getepisodeslist_request_data.returncode;
			switch (getepisodeslistCode) {
			case 00:
				return {
				totalcount:getepisodeslist_request_data.totalcount,
				programcode:getepisodeslist_request_data.programcode, // VOD program code，if it is series, the value is series head code 
				programname:getepisodeslist_request_data.programname, // VOD program name 
				programnamelen:getepisodeslist_request_data.programnamelen, // VOD program name length 
				programtype:getepisodeslist_request_data.programtype, // VOD program type, 1: vod program, 9: Ads, 10: One episode of the series, 14: Series head 
				contentcode:getepisodeslist_request_data.contentcode, // Content code, series head is series head code. 
				seriesprogramcode:getepisodeslist_request_data.seriesprogramcode, // Series head program code, Single-episode series program corresponding series head code 
				isrecommend:getepisodeslist_request_data.isrecommend, // Recommend or not, 0: No, 1: Yes, Note: it is from metadata, it is different from recommend data. 
				ishot:getepisodeslist_request_data.ishot, // Hot or not, 0: No, 1: Yes, Note: it is from metadata, it is different from hot data. 
				isfirstpage:getepisodeslist_request_data.isfirstpage, // First page poster or not, 0: No, 1: Yes 
				catagorycode:getepisodeslist_request_data.catagorycode, // Column code of CP 
				bocode:getepisodeslist_request_data.bocode, // Service operator code 
				programsearchkey:getepisodeslist_request_data.programsearchkey, // Program name search keywords (program name initials). 
				mediaservices:getepisodeslist_request_data.mediaservices, // Collection of media service type(supporting the multiple attributes). see General Explanatory Notes. 
				ratingid:getepisodeslist_request_data.ratingid, // Rating ID, see General Explanatory Notes. 
				recommendid:getepisodeslist_request_data.recommendid, // Recommend ID 
				sortnum:getepisodeslist_request_data.sortnum, // Sort number 
				price:getepisodeslist_request_data.price, // Price, unit: cent 
				enabledtime:getepisodeslist_request_data.enabledtime, // Enabled time, format:YYYY.MM.DD HH:24MI:SS 
				disabledtime:getepisodeslist_request_data.disabledtime, // Disabled time, format:YYYY.MM.DD HH:24MI:SS 
				onlinetime:getepisodeslist_request_data.onlinetime, // On-line time, format:YYYY.MM.DD HH:24MI:SS 
				offlinetime:getepisodeslist_request_data.offlinetime, // Off-line time, format:YYYY.MM.DD HH:24MI:SS 
				createtime:getepisodeslist_request_data.createtime, // Creation time, format:YYYY.MM.DD HH:24MI:SS 
				countryname:getepisodeslist_request_data.countryname, // Country name 
				issimpletrailer:getepisodeslist_request_data.issimpletrailer, // Simple trailer or not, 0: No, 1: Yes 
				telecomcode:getepisodeslist_request_data.telecomcode, // External code 
				mediacode:getepisodeslist_request_data.mediacode, // Media provider code 
				trailerbegintime:getepisodeslist_request_data.trailerbegintime, // Simple trailer start time, format: HH:MM:SS 
				trailerendtime:getepisodeslist_request_data.trailerendtime, // Simple trailer end time, format: HH:MM:SS 
				seriesnum:getepisodeslist_request_data.seriesnum, // Number of episodes of the series, 1 by default for non-series. 
				posterfilelist:getepisodeslist_request_data.posterfilelist, // Poster name list.This field contains 12 posters separated by ; . Poster arranging order is from IPTV, MVS-Mobile, PC to MVS-Tablets, each screen with three posters including normal poster, smaller poster and big poster. This field is splited into poster1 to poster12 field. 
				posterpath:getepisodeslist_request_data.posterpath, // Relative path of poster, default is ../images/poster/. 
				wggenre:getepisodeslist_request_data.wggenre, // Content style type of WenGuang content provider 
				wgkeywords:getepisodeslist_request_data.wgkeywords, // Keywords of WenGuang content provider
				wgtags:getepisodeslist_request_data.wgtags, // Tags of WenGuang content provider 
				description:getepisodeslist_request_data.description, // Program description 
				director:getepisodeslist_request_data.director, // Director name list, separate by ‘;’ 
				directorsearchkey:getepisodeslist_request_data.directorsearchkey, // Director name initials list, separate by ‘;’ 
				actor:getepisodeslist_request_data.actor, // Actor/actress name list, separate by ‘;’ 
				actorsearchkey:getepisodeslist_request_data.actorsearchkey, // Actor/actress name initial list, separate by ’ ;’ 
				cpcode:getepisodeslist_request_data.cpcode, // Content provider code
				cpname:getepisodeslist_request_data.cpname, // Content provider name 
				columncode:getepisodeslist_request_data.columncode, // Column code 
				catalogname:getepisodeslist_request_data.catalogname, // Content type name, such as music 
				descriptionkey:getepisodeslist_request_data.descriptionkey, // Content description keywords 
				advertisecontent:getepisodeslist_request_dataadvertisecontent., // Support ads or not, 0: No, >0: Collection of media service type. see General Explanatory Notes. 
				releasedate:getepisodeslist_request_data.releasedate, // Release date 
				writer:getepisodeslist_request_data.writer, // Writer list, separate by ; 
				audiolang:getepisodeslist_request_data.audiolang, // Audio language information list, separate by ‘;’ 
				subtitlelang:getepisodeslist_request_data.subtitlelang, // Subtitle language information list, separate by ‘;’ 
				pubcompany:getepisodeslist_request_data.pubcompany, // Publish company name 
				detaildescribed:getepisodeslist_request_data.detaildescribed, // Detail program description information(optional field) 
				seriesseason:getepisodeslist_request_data.seriesseason, // Number of seasons 
				genre:getepisodeslist_request_data.genre, // Genre list, separate by ‘;’, such as fantasy and action
				licenseperiod:getepisodeslist_request_data.licenseperiod, // Lisence plate valid period 
				channelcode:getepisodeslist_request_data.channelcode, // Channel code of the archived program 
				subgenre:getepisodeslist_request_data.subgenre, // Sub style type of content, such as fantasy and action 
				shortdesc:getepisodeslist_request_data.shortdesc, // Aspect, a brief plot description 
				shorttitle:getepisodeslist_request_data.shorttitle, // Subtitle 
				elapsedtime:getepisodeslist_request_data.elapsedtime, // Length of program, format: HH:MI:SS 
				starlevel:getepisodeslist_request_data.starlevel, // Star level, value:0-10 
				istimeshift:getepisodeslist_request_data.istimeshift, // weather support to timeshift 1:yes，0:no 
				isarchivemode:getepisodeslist_request_data.isarchivemode, 
				isprotection:getepisodeslist_request_data.isprotection,
				timeshiftmode:getepisodeslist_request_data.timeshiftmode, 
				archivemode:getepisodeslist_request_data.archivemode, 
				copyprotection:getepisodeslist_request_data.copyprotection, 
				language:getepisodeslist_request_data.language, 
				format:getepisodeslist_request_data.format, 
				aspect:getepisodeslist_request_data.aspect, 
				dolby:getepisodeslist_request_data.dolby, 
				episodetitle:getepisodeslist_request_data.episodetitle, 
				parentaladvisory:getepisodeslist_request_data.parentaladvisory, 
				catalogtype:getepisodeslist_request_data.catalogtype, 
				seriestype:getepisodeslist_request_data.seriestype,
			};
				
				break;

			default:
				break;
			}
		}
	};
	
}