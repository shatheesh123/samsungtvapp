/**
 * Querying VOD Program Play URL
 */

function getvodurl(ipadd,port,programcode,breakpoint,authidsession,definition,mediaservice) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/getvodurl.jsp?programcode="+programcode+"&breakpoint="+breakpoint+"&authidsession="+authidsession+"&definition="+definition+"&mediaservice="+mediaservice;
	var getvodurl_request=new XMLHttpRequest();
	getvodurl_request.open("GET", url, true);
	getvodurl_request.send()
	getvodurl_request.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			var getvodurl_request_data=JSON.parse(getvodurl_request.rseponseText);
			var vodCode=parseInt(getvodurl_request_data.returncode);
			switch (vodCode) {
			case 00:
				return{
					errormsg : getvodurl_request_data.errormsg,
					url: getvodurl_request_data.url
						};
				break;
			default:
				return null;
				break;
			}
		}
	};
}