/**
 * Querying Catch-up Play URL
 */

function getcaturl(ipadd,port,prevuecode,channelcode,authidsession,breakpoint,definition,mediaservice) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/gettvodplayurl.jsp?prevuecode="+prevuecode+"&channelcode="+channelcode+"&authidsession="+authidsession+"&breakpoint="+breakpoint+"&definition="+definition+"&authidsession="+mediaservice;
	var catchup_request=new XMLHttpRequest();
	catchup_request.open("GET", url,true);
	catchup_request.send();
	catchup_request.onreadystatechange=function(){
		if(catchup_request.readyState==4 && catchup_request.status==200){
			var catchup_request_data=JSON.parse(catchup_request.responseText);
			var catchupCode=parseInt(catchup_request_data.returncode);
			switch (catchupCode) {
			case 00:
				return{
				errormsg : catchup_request_data.errormsg,
				url: catchup_request_data.url
				};
				break;

			default:
				return null;
				break;
			}
		}
	};
}