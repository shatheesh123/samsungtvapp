/**
 * cookie control panel
 */


//to set cookie keys and values
function setcookievalue(ckey,cvalue) {
	localStorage.setItem(ckey,cvalue);
}

//to get specific cookie vales.
function getcookievalue(ckey) {
	var status;
	localData=localStorage.getItem(ckey);
	try{
	if(localStorage.getItem(ckey)!=null){
		status=true;
		return{
			localData:localData,
			status:status
		};
	}
	else{
		status=false;
		return{
			localData:localData,
			status:status
		};
	}
	}
	catch(err){
		status=false;
		return{
			localData:localData,
			status:status
		};
}
}
	


//to delete specific cookie storage or clear all
function deletecookievalues(ckey,clearAll) {
	try{
	if(clearAll){
		localStorage.clear();
	}
	localStorage.removeItem(ckey);
	}
	catch(err){
		return null;
	}
}