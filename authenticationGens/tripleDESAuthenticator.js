/**
 * 3DES Security Encryption
 * 
 */
var code;

//3DES Encryption
function authcodeEnryption(EncrytToken,UserID,stbid,ip,mac,reserved,licenceServerCode) {
	var ranNum=Math.floor(Math.random()*20);
	var threeDesToken=ranNum+"$"+EncrytToken+"$"+UserID+"$"+stbid+"$"+ip+"$"+mac+"$"+reserved+"$"+"CTC";
	var licenceServerCodeHex=CryptoJS.enc.Hex.parse(licenceServerCode);
//	console.log("tokenGen: "+threeDesToken);
	code=licenceServerCodeHex;
	var encrypted = CryptoJS.TripleDES.encrypt(threeDesToken, licenceServerCodeHex,{mode : CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
	return encrypted;
}


//3DES Decryption
function authcodeDecryption(cipherCode,decryptCode){
	try{
		var decrypted=CryptoJS.TripleDES.decrypt(cipherCode, decryptCode,{mode : CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
	}
	catch(e){
		return null;
	}
	return decrypted;
}



