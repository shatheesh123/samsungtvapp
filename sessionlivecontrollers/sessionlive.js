/**
 * Session Keep-alive Interface
 */

function heartbeat(ipadd,port,playingigmp) {
	var url="http://"+ipadd+":"+port+"/iptvepg/datasource/heartbeat.jsp?playingigmp="+playingigmp;
	var heartbeat_request= new XMLHttpRequest();
	heartbeat_request.open("GET", url, true);
	heartbeat_request.send();
	heartbeat_request.onreadystatechange=function(){
		if(heartbeat_request.readyState==4 && heartbeat_request==200){
			heartbeat_request_data=JSON.parse(heartbeat_request.responseText);
			heartbeatCode=parseInt(heartbeat_request_data.returncode);
			switch (heartbeatCode) {
			case 00:
				return{
				newusertoken:heartbeat_request_data.newusertoken,
				expiretime:heartbeat_request_data.expiretime	
			};				
				break;

			default:
				break;
			}
		}
	};	
}