var Main = {};
var video_url="http://clips.vorwaerts-gmbh.de/VfE_html5.mp4";
var global_get_url;
//var video_url="http://inoveonline.pt/videos_rasp/1.mp4";
//var video_url="http://commondatastorage.googleapis.com/wvmedia/sintel_main_720p_4br_tp.wvm";
var request_data;
var post_request_data;
var newTime;
var global_count=1;
var global_duration;
var drmEvent_global;
var drmData_global;
var removableId;


//to set videoplay speed
function setspeed(posspeed){
	if(posspeed!=1 || posspeed!=-1){
	webapis.avplay.setSpeed(posspeed);
	document.getElementById("videospeed").innerHTML="Playspeed : "+posspeed+"x";
	}
	else{
		document.getElementById("videospeed").innerHTML="Normal Speed";
		webapis.avplay.setSpeed(0);
	}
}

//function onStorageStateChanged(storage){
//	if(storage.state=="MOUNTED"){
//		console.log("Storage "+storage.label+" was added.")
//		tizen.fileSystem.removeStorageStateChangeListner(removableId);
//	}
//	alert("plugged");
//}

function onStorageStateChanged(storage){
	console.log("plugged");
	
}

//remove usb error notification
function usbcheck(){
	document.getElementById("backImage").style.visibility='hidden';
	
	}

//if usb is successfully plugged
function onsuccessusb() {
	document.getElementById("backImage").style.visibility='visible';
	AVPlayer.stop();
	console.log("USB Loaded");
}

//if usb is not plugged correctly
function onerrorusb(){
	console.log("USB Error");
}


//var drmParam={LicenceServer: "licence url",
//	DeleteLicenceAfterUse:true};
//function setDrmParameters(paramObject) {
//	webapis.avplay.setDrm("PLAYREADY","SetProperties",JSON.stringify(paramObject));
//}



//to decrease the speed of video
function setspeeddown(){
	local_count_down=global_count;
	if(1<local_count_down && local_count_down<32){
		local_count_down=local_count_down/2;
		global_count=local_count_down;
	return local_count_down;
	}
	if(-32<local_count_down && local_count_down<=1){
		if(local_count_down>0){
		local_count_down= -1*local_count_down;
		}
		local_count_down= local_count_down*2;
		global_count=local_count_down;
		return local_count_down;
		}
	
	else{
		local_count_down=1;
		global_count=local_count_down;
		return local_count_down;
	}
	
}

//to increase the speed of video
function setspeedup(){
	local_count_up=global_count;
	if(-32<local_count_up && local_count_up<-1){
		local_count_up=local_count_up/2;
		global_count=local_count_up;
		return local_count_up;
	}
	if(-1<=local_count_up && local_count_up<32){
		if(local_count_up<0){
			local_count_up=Math.abs(local_count_up);
		}
		local_count_up=local_count_up*2;
		global_count=local_count_up;
		return local_count_up;
	}
	else{
		local_count_up=-1;
		global_count=local_count_up;
		return local_count_up;
	}
}


//Async successCallback while playing the video
var successCallback=function(){
	global_duration=webapis.avplay.getDuration();
	document.getElementById("total-time").innerHTML=global_duration/1000;
}

//Async errorCallback while playing the video
var errorCallback=function(){
	return null;
}

//to set cookie keys and values
//function setcookievalue(ckey,cvalue) {
//	sessionStorage.setItem(ckey,cvalue);
//}

////to get specific cookie vales.
//function getcookievalue(ckey) {
//	try{
//	var localData=sessionStorage.getItem(ckey);
//	return localData;
//	}
//	catch(err){
//		return null;
//		
//	}
//}
//
////to delete specific cookie storage or clear all
//function deletecookievalues(ckey,clearAll) {
//	try{
//	if(clearAll){
//		sessionStorage.clear();
//	}
//	sessionStorage.removeItem(ckey);
//	}
//	catch(err){
//		return null;
//	}
//}


/**
@param storage;
*/


Main.onLoad = function () {
	console.log("Main.onLoad()");
	Main.enableMediaKeys();
	Main.handleKeyDownEvents();
	document.getElementById("video_url_id").innerHTML=video_url;
	document.getElementById("json_url_id").innerHTML=global_get_url;
	tizen.filesystem.addStorageStateChangeListener(onStorageStateChanged);

// Test codes are here. initial state.onLoaded()
	consoleTest("SmartTV application is Started");
	consoleTest("***************Test is Started!******************");
//	loginflowTest();
//	var tripleEnc=authcodeEnryption("samsung","tizen","1","102.21.14.4","aE:23:sa:12","true","121ASAF34");
//	console.log("EncStr: ", tripleEnc.toString());
//	console.log("PassCode: ", code.toString());
////	var testHex=hexadecgenerator(tripleEnc.toString());
////	console.log("EncStrHex: "+testHex);
//	var tripleDec=authcodeDecryption(tripleEnc, code);
//	console.log("DecStr", tripleDec.toString(CryptoJS.enc.Utf8));
	consoleTest("***************Test is Ended!********************");
//Test phase is OVER
	
	
	AVPlayer.init("av-player");
	AVPlayer.prepare(video_url);
	
 
}//closed
Main.onUnload = function () {
	console.log("Main.onUnload()");
}

//security privillages for remote keys
Main.enableMediaKeys = function () {	
	tizen.tvinputdevice.registerKey("MediaPlayPause");
	tizen.tvinputdevice.registerKey("MediaPlay");
	tizen.tvinputdevice.registerKey("MediaStop");
	tizen.tvinputdevice.registerKey("MediaPause");
	tizen.tvinputdevice.registerKey("MediaRewind");
	tizen.tvinputdevice.registerKey("MediaFastForward");	
}

Main.handleKeyDownEvents = function () {

    document.addEventListener('keydown', function(e) {
    	    	
    	switch(e.keyCode){
    	case tvKey.LEFT: //LEFT arrow
        	console.log("LEFT");
    		break;
    	case tvKey.UP: //UP arrow
    		console.log("UP");
    		break;
    	case tvKey.RIGHT: //RIGHT arrow
    		console.log("RIGHT");
    		break;
    	case tvKey.DOWN: //DOWN arrow
    		console.log("DOWN");
    		break;
    	case tvKey.ENTER: //OK button
    		console.log("OK");
    		AVPlayer.setDisplayArea(0, 0, 1920, 1080);
    		break;
    	case tvKey.RETURN: //RETURN button
    		console.log("RETURN");
    		AVPlayer.setDisplayArea(0, 0, 960, 540);
    		break;
    	case tvKey.PLAYPAUSE: // PLAYPAUSE button
    		console.log("PLAYPAUSE");
    		if (AVPlayer.state == AVPlayer.STATES.PLAYING) {
    			AVPlayer.pause();
    		} else {
    			AVPlayer.play();
    		}    		
    		break;
    	case tvKey.PLAY: // PLAY button
    		console.log("PLAY");
    		usbcheck();
    		AVPlayer.play();
    		setspeed(1);

    		break;
    	case tvKey.PAUSE: // PAUSE button
    		console.log("PAUSE");
    		AVPlayer.pause();
    		break;
    	case tvKey.FF:
    		console.log("FASTFORWARD");
    		var posspeedstep=setspeedup();
    		setspeed(posspeedstep);
    		break;
    	case tvKey.RW:
    		console.log("REWIND");
    		var negspeedstep=setspeeddown();
    		console.log(negspeedstep);
    		setspeed(negspeedstep);
    		break;	
    	case tvKey.STOP:
    		AVPlayer.stop();
    		Main.onLoad();
      		console.log("STOP");
    		break;
    	case tvKey.RED:
    		console.log("RED");
//    		tizen.filesystem.listStorages(function(result){
//    			console.log(JSON.stringify(result));
//    		},function(){
//    			console.log(JSON.stringify(error));
//    		});
    		tizen.filesystem.listStorages(onsuccessusb,onerrorusb);
    		deletecookievalues("pause");
    		break;
    	default:
    		console.log("Key code : " + e.keyCode);
    		console.log("default");
    		break;
    	}
    });
}

window.onload = Main.onLoad;
window.onunload = Main.onUnload;

//init_the_player

(function(){
	
	var AVPlayer = {
		videoObj: null,
		STATES: {
	        STOPPED: 0,
	        PLAYING: 1,
	        PAUSED: 2, 
	        PREPARED: 4
	    },
	    state: 0		
	};
	
	// Initialize_player
	AVPlayer.init = function (id) {
		
		console.log("Player.init("+id+")");
		console.log("AVPlay version: " + webapis.avplay.getVersion());
		
		this.state = this.STATES.STOPPED;
		
		if (!this.videoObj && id) {
			this.videoObj = document.getElementById(id);
		}
	}
	
	// Load_stream
	AVPlayer.prepare = function (url) {
		
		console.log("Player.prepare("+url+")");		
		
		if (this.state > this.STATES.STOPPED) {
			return;
		}
		
		if (!this.videoObj) {
			return 0;
		}
		
		webapis.avplay.open(url);
		this.setupEventListeners();
		this.setDisplayArea(0,0,1920,1080);
		webapis.avplay.setStreamingProperty("SET_MODE_4K") //for 4K contents			
		webapis.avplay.prepare();
		
		this.state = this.STATES.PREPARED;
	}
	
	// Play_stream
	AVPlayer.play = function (url) {
		
		console.log("Player.play("+url+")");
		
		if (this.state < this.STATES.PAUSED) {
			return;
		}
		
		this.state = this.STATES.PLAYING;
		
		if (url) {
			this.videoObj.src = url;
		}
		
		webapis.avplay.play();		
	}
	
	// Pause_stream
	AVPlayer.pause = function () {
		
		console.log("Player.pause()");
		
		if (this.state != this.STATES.PLAYING) {
			return;
		}
		
		this.state = this.STATES.PAUSED;
		
		webapis.avplay.pause();
				
	}
	
	// Stop_stream
	AVPlayer.stop = function () {
		
		console.log("Player.stop()");
		
		this.state = this.STATES.STOPPED;		
		webapis.avplay.stop();
	}
	

	AVPlayer.setDisplayArea = function (x, y, width, height) {		
		webapis.avplay.setDisplayRect(x,y,width,height);
	}
	

	AVPlayer.formatTime = function (seconds) {
				
		var hh = Math.floor(seconds / 3600),
   		    mm = Math.floor(seconds / 60) % 60,
		    ss = Math.floor(seconds) % 60;
		  
		return (hh ? (hh < 10 ? "0" : "") + hh + ":" : "") + 
			   ((mm < 10) ? "0" : "") + mm + ":" + 
			   ((ss < 10) ? "0" : "") + ss;			
	}
	

	AVPlayer.setupEventListeners = function () {
	
		var that = this;
		
		var listener = {
			onbufferingstart: function() {
				console.log("Buffering...");
			},
			onbufferingprogress: function(percent) {
				console.log("Buffering progress: " + percent);
			},
			onbufferingcomplete: function() {
				try{
				console.log('Buffering Complete, Can play now!');
				webapis.avplay.prepareAsync(successCallback,errorCallback);
				}
				catch(err){
					alert("within onbufferingcomplete");
					return null;
				}
			},
			onstreamcompleted: function() {
				console.log('video has ended.');
				webapis.avplay.stop();
				that.state = that.STATES.STOPPED;
//				Main.onload();
				document.getElementById('progress-amount').style.width = "100%";
			},
			oncurrentplaytime: function(currentTime) {				
				var duration =  webapis.avplay.getDuration();	
				global_duration=duration;
			    if (duration > 0) {			    	
			    	var percent = ((currentTime / duration)*100);		    	
			    	document.getElementById('progress-amount').style.width = percent + "%";
			    	document.getElementById("current-time").innerHTML = that.formatTime(currentTime/1000);
			    }		
			    
			},
			ondrmevent: function(drmEvent, drmData) {
				console.log("DRM callback: " + drmEvent + ", data: " + drmData);
				drmEvent_global=drmEvent;
				drmData_global=drmData;
			},			
			onerror : function(type, data) {
				console.log("OnError: " + data);
			}
	    }
		
		webapis.avplay.setListener(listener);
        
	}	
	
	if (!window.AVPlayer) {
		window.AVPlayer = AVPlayer;
	}	
	
})()
